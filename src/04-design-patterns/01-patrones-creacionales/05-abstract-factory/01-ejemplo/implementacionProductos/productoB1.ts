import { IProductoB } from "../interfaces/productB";

export class ProductoB1 implements IProductoB {
  operacionB(): string {
    return 'Producto B1';
  }
}