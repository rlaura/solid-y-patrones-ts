import { IProductoB } from "../interfaces/productB";

export class ProductoB2 implements IProductoB {
  operacionB(): string {
    return 'Producto B2';
  }
}