import { IProductoA } from "../interfaces/productA";

// Implementación de una familia de productos concretos
export class ProductoA2 implements IProductoA {
  operacionA(): string {
    return 'Producto A2';
  }
}