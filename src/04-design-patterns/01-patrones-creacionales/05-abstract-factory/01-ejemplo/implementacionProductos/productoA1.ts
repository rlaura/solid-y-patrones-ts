import { IProductoA } from "../interfaces/productA";

// Implementación de una familia de productos concretos
export class ProductoA1 implements IProductoA {
  operacionA(): string {
    return 'Producto A1';
  }
}