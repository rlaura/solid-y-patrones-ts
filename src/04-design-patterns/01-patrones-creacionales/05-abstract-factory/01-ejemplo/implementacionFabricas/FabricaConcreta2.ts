import { ProductoA2 } from "../implementacionProductos/productoA2";
import { ProductoB2 } from "../implementacionProductos/productoB2";
import { IFabricaAbstracta } from "../interfaces/fabricaAbstracta";
import { IProductoA } from "../interfaces/productA";
import { IProductoB } from "../interfaces/productB";


// Implementación de la fábrica concreta para la familia 1
export class FabricaConcreta2 implements IFabricaAbstracta {
  crearProductoA(): IProductoA {
    return new ProductoA2();
  }

  crearProductoB(): IProductoB {
    return new ProductoB2();
  }
}