import { ProductoA1 } from "../implementacionProductos/productoA1";
import { ProductoB1 } from "../implementacionProductos/productoB1";
import { IFabricaAbstracta } from "../interfaces/fabricaAbstracta";
import { IProductoA } from "../interfaces/productA";
import { IProductoB } from "../interfaces/productB";

// Implementación de la fábrica concreta para la familia 1
export class FabricaConcreta1 implements IFabricaAbstracta {
  crearProductoA(): IProductoA {
    return new ProductoA1();
  }

  crearProductoB(): IProductoB {
    return new ProductoB1();
  }
}