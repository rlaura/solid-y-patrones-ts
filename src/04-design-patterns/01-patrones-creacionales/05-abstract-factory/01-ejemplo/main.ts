import { FabricaInit } from "./fabricaInit";
import { FabricaConcreta1 } from "./implementacionFabricas/FabricaConcreta1";
import { FabricaConcreta2 } from "./implementacionFabricas/FabricaConcreta2";
import { IFabricaAbstracta } from "./interfaces/fabricaAbstracta";

function main() {
  //inicializamos el creador de fabricas
  const fabricaInit = new FabricaInit();
  // Ejemplo de uso con la familia 1 de productos
  const fabrica1: IFabricaAbstracta = new FabricaConcreta1();
  fabricaInit.getFabrica(fabrica1);
  console.log("======================================");
  // Ejemplo de uso con la familia 2 de productos
  const fabrica2: IFabricaAbstracta = new FabricaConcreta2();
  fabricaInit.getFabrica(fabrica2);
}
main();
