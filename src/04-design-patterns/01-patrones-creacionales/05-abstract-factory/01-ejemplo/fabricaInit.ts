import { IFabricaAbstracta } from "./interfaces/fabricaAbstracta";

export class FabricaInit {
  getFabrica(fabrica: IFabricaAbstracta) {
    const productoA = fabrica.crearProductoA();
    const productoB = fabrica.crearProductoB();

    console.log(productoA.operacionA());
    console.log(productoB.operacionB());
  }
}
