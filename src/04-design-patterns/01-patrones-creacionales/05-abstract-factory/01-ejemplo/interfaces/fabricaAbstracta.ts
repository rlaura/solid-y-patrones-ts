import { IProductoA } from "./productA";
import { IProductoB } from "./productB";

// Definición de la fábrica abstracta
export interface IFabricaAbstracta {
  crearProductoA(): IProductoA;
  crearProductoB(): IProductoB;
}