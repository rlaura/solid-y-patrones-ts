import { IFabricaRegion } from "./interfaces/fabricaRegion";


export class CrearFabrica {

  getFabrica(fabrica: IFabricaRegion) {
    const metodoPago = fabrica.crearMetodoPago();
    const calculoImpuestos = fabrica.crearCalculoImpuestos();
    const moneda = fabrica.crearMoneda();

    console.log("Configurando sistema para la región:");
    metodoPago.procesarPago();
    calculoImpuestos.calcularImpuestos();
    console.log(`Utilizando moneda: ${moneda.obtenerSimbolo()}`);
  }
}
