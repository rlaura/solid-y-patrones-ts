import { CalculoImpuestosEuropeo } from "../implementacionEuropa/calculoImpuestosEuropeo";
import { MetodoPagoEuropeo } from "../implementacionEuropa/metodoPagoEuropeo";
import { MonedaEuro } from "../implementacionEuropa/monedaEuro";
import { ICalculoImpuestos } from "../interfaces/calculoImpuestos";
import { IFabricaRegion } from "../interfaces/fabricaRegion";
import { IMetodoPago } from "../interfaces/metodoPago";
import { IMoneda } from "../interfaces/moneda";


// Implementación de la fábrica concreta para la región Europa
export class FabricaEuropea implements IFabricaRegion {
  crearMetodoPago(): IMetodoPago {
    return new MetodoPagoEuropeo();
  }

  crearCalculoImpuestos(): ICalculoImpuestos {
    return new CalculoImpuestosEuropeo();
  }

  crearMoneda(): IMoneda {
    return new MonedaEuro();
  }
}