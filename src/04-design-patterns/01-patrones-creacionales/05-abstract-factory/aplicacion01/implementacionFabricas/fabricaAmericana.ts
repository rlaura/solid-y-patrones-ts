import { CalculoImpuestosAmericano } from "../implementacionAmerica/calculoImpuestosAmericano";
import { MetodoPagoAmericano } from "../implementacionAmerica/metodoPagoAmericano";
import { MonedaAmericana } from "../implementacionAmerica/monedaAmericana";
import { ICalculoImpuestos } from "../interfaces/calculoImpuestos";
import { IFabricaRegion } from "../interfaces/fabricaRegion";
import { IMetodoPago } from "../interfaces/metodoPago";
import { IMoneda } from "../interfaces/moneda";


// Implementación de la fábrica concreta para la región América
export class FabricaAmericana implements IFabricaRegion {
  crearMetodoPago(): IMetodoPago {
    return new MetodoPagoAmericano();
  }

  crearCalculoImpuestos(): ICalculoImpuestos {
    return new CalculoImpuestosAmericano();
  }

  crearMoneda(): IMoneda {
    return new MonedaAmericana();
  }
}
