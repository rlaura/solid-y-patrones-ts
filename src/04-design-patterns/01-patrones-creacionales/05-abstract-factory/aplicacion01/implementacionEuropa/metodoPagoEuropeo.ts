import { IMetodoPago } from "../interfaces/metodoPago";


// Implementación de productos concretos para la región Europa
export class MetodoPagoEuropeo implements IMetodoPago {
  procesarPago(): void {
    console.log('Procesando pago en Europa');
  }
}