import { ICalculoImpuestos } from "../interfaces/calculoImpuestos";


export class CalculoImpuestosEuropeo implements ICalculoImpuestos {
  calcularImpuestos(): void {
    console.log('Calculando impuestos en Europa');
  }
}