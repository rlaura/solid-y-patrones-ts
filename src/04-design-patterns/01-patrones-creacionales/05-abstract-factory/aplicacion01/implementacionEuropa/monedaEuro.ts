import { IMoneda } from "../interfaces/moneda";


export class MonedaEuro implements IMoneda {
  obtenerSimbolo(): string {
    return '€';
  }
}