import { CrearFabrica } from "./crearFabrica";
import { FabricaAmericana } from "./implementacionFabricas/fabricaAmericana";
import { FabricaEuropea } from "./implementacionFabricas/fabricaEuropea";
/**
 * TODO: Vamos a imaginar un escenario en el que estás construyendo un sistema 
 * de comercio electrónico que puede funcionar en diferentes regiones del mundo. 
 * En este caso, podrías utilizar el patrón de fábrica abstracta para gestionar 
 * la creación de objetos específicos para cada región, como métodos de pago, 
 * cálculos de impuestos y monedas.
 */
function main() {
  //inicializamos el creador de fabricas
  const fabricaAmericana = new FabricaAmericana();

  // Ejemplo de uso para la región América
  const fabricaInit = new CrearFabrica();
  fabricaInit.getFabrica(fabricaAmericana);
  console.log("======================================")

  // Ejemplo de uso para la región Europa
  const fabricaEuropea = new FabricaEuropea();
  fabricaInit.getFabrica(fabricaEuropea);
}
main();
