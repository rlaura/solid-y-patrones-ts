import { IMoneda } from "../interfaces/moneda";


export class MonedaAmericana implements IMoneda {
  obtenerSimbolo(): string {
    return '$';
  }
}