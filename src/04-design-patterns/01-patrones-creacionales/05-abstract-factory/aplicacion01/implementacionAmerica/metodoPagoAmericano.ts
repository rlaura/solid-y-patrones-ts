import { IMetodoPago } from "../interfaces/metodoPago";

// Implementación de productos concretos para la región América
export class MetodoPagoAmericano implements IMetodoPago {
  procesarPago(): void {
    console.log('Procesando pago en América');
  }
}