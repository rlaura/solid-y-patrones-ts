import { ICalculoImpuestos } from "../interfaces/calculoImpuestos";


export class CalculoImpuestosAmericano implements ICalculoImpuestos {
  calcularImpuestos(): void {
    console.log('Calculando impuestos en América');
  }
}