import { ICalculoImpuestos } from "./calculoImpuestos";
import { IMetodoPago } from "./metodoPago";
import { IMoneda } from "./moneda";


// Definición de la fábrica abstracta para productos relacionados con regiones
export interface IFabricaRegion {
  crearMetodoPago(): IMetodoPago;
  crearCalculoImpuestos(): ICalculoImpuestos;
  crearMoneda(): IMoneda;
}
