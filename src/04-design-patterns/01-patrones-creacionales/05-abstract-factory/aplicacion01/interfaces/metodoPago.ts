// Definición de interfaces para productos relacionados con regiones
export interface IMetodoPago {
  procesarPago(): void;
}