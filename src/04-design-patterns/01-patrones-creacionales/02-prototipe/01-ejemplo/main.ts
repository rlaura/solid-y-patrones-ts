import { MiPrototipo } from "./MiPrototipo";


function main(): void {
  // Uso del patrón Prototype
  const prototipoOriginal = new MiPrototipo("Valor1", 42);

  // Clonar el prototipo para crear nuevos objetos
  const copia1 = prototipoOriginal.clone();
  const copia2 = prototipoOriginal.clone();

  // Verificar que las copias son diferentes al original
  console.log(copia1 !== prototipoOriginal); // true
  console.log(copia2 !== prototipoOriginal); // true

  // Verificar que las copias tienen los mismos valores que el original
  console.log(copia1.obtenerPropiedad1()); // "Valor1"
  console.log(copia1.obtenerPropiedad2()); // 42

  console.log(copia2.obtenerPropiedad1()); // "Valor1"
  console.log(copia2.obtenerPropiedad2()); // 42
}

main();
