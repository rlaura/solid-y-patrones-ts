interface Clonable {
  clone(): Clonable;
}

export class MiPrototipo implements Clonable {
  private propiedad1: string;
  private propiedad2: number;

  constructor(propiedad1: string, propiedad2: number) {
    this.propiedad1 = propiedad1;
    this.propiedad2 = propiedad2;
  }

  // Método para clonar el objeto
  clone(): MiPrototipo {
    return new MiPrototipo(this.propiedad1, this.propiedad2);
  }

  // Otros métodos y propiedades según sea necesario
  obtenerPropiedad1(): string {
    return this.propiedad1;
  }

  obtenerPropiedad2(): number {
    return this.propiedad2;
  }
}
