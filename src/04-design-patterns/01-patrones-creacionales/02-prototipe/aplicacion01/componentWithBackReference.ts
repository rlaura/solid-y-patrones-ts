import Prototype from "./prototype";


export class ComponentWithBackReference {
  public prototype;

  constructor(prototype: Prototype) {
      this.prototype = prototype;
  }
}
//TODO: prueba esta implementacion sin el export defaul en todos las demas clases y el main
// y mostrara un error 
export default ComponentWithBackReference;