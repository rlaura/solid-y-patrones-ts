import ComponentWithBackReference from "./componentWithBackReference";

/**
 * La clase de ejemplo que tiene capacidad de clonación. Veremos cómo los valores del campo
 * con diferentes tipos serán clonados.
 */
export class Prototype {
  public primitive: any;
  //TODO: usamos el simbolo de exclamacion para indicarle a typescript que confie en nosotros
  // que siempre le enviaremos un dato para inicializar
  public component!: Object;
  public circularReference!: ComponentWithBackReference;

  public clone(): this {
    const clone = Object.create(this);

    clone.component = Object.create(this.component);

    // Clonar un objeto que tiene un objeto anidado con referencia inversa
    // requiere un tratamiento especial. Una vez completada la clonación, el
    // el objeto anidado debe apuntar al objeto clonado, en lugar del
    // objeto original. El operador de extensión puede resultar útil en este caso.
    clone.circularReference = {
      ...this.circularReference,
      prototype: { ...this },
    };

    return clone;
  }
}

//TODO: prueba esta implementacion sin el export defaul en todos las demas clases y el main
// y mostrara un error
export default Prototype;
