import ComponentWithBackReference from "./componentWithBackReference";
import Prototype from "./prototype";

/**
 * The client code.
 */
function clientCode() {
  const p1 = new Prototype();
  p1.primitive = 245;
  p1.component = new Date();
  p1.circularReference = new ComponentWithBackReference(p1);

  const p2 = p1.clone();
  if (p1.primitive === p2.primitive) {
    console.log(
      "Los valores de campo primitivos se han transferido a un clon. ¡Hurra!"
    );
  } else {
    console.log(
      "Los valores de los campos primitivos no se han copiado. ¡Buuuu!"
    );
  }
  if (p1.component === p2.component) {
    console.log("El componente simple no ha sido clonado. ¡Buuuu!");
  } else {
    console.log("Se ha clonado un componente simple. ¡Hurra!");
  }

  if (p1.circularReference === p2.circularReference) {
    console.log(
      "El componente con referencia anterior no ha sido clonado. ¡Buuuu!"
    );
  } else {
    console.log("Se ha clonado el componente con referencia anterior. ¡Hurra!");
  }

  if (p1.circularReference.prototype === p2.circularReference.prototype) {
    console.log(
      "El componente con referencia anterior está vinculado al objeto original. ¡Buuuu!"
    );
  } else {
    console.log(
      "El componente con referencia anterior está vinculado al clon. ¡Hurra!"
    );
  }
}

clientCode();
