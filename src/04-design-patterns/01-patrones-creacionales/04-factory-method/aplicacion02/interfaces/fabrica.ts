import { Empleado } from "./empleado";

// Definición de la interfaz de la fábrica de empleados
export interface FabricaEmpleados {
  crearEmpleado(): Empleado;
}