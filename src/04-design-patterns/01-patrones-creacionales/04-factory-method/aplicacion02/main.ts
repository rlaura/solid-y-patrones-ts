import { FabricaEmpleadoTiempoCompleto } from "./implementacionFabricas/fabricaEmpleadoTiempoCompleto";
import { FabricaEmpleadoTiempoParcial } from "./implementacionFabricas/fabricaEmpleadoTiempoParcial";
import { Empleado } from "./interfaces/empleado";
import { FabricaEmpleados } from "./interfaces/fabrica";

/**
 * TODO:
 * Supongamos que estás trabajando en un sistema de gestión de recursos humanos (RH) 
 * y necesitas gestionar diferentes tipos de empleados. Puedes usar una fábrica para 
 * crear instancias específicas de empleados según su tipo, como "Empleado de Tiempo Completo" 
 * o "Empleado a Tiempo Parcial":
 */
function main() {
  // Uso de la fábrica en el sistema de recursos humanos
  const fabricaEmpleadoTC: FabricaEmpleados = new FabricaEmpleadoTiempoCompleto();
  const empleadoTC: Empleado = fabricaEmpleadoTC.crearEmpleado();
  // Salida: Empleado a Tiempo Completo
  console.log(empleadoTC.obtenerDetalle());

  const fabricaEmpleadoTP: FabricaEmpleados = new FabricaEmpleadoTiempoParcial();
  const empleadoTP: Empleado = fabricaEmpleadoTP.crearEmpleado();
  // Salida: Empleado a Tiempo
  console.log(empleadoTP.obtenerDetalle());
}
main();
