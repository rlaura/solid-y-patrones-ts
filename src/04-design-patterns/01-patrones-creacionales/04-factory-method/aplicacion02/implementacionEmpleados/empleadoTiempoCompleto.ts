import { Empleado } from "../interfaces/empleado";

// Implementación de dos tipos de empleados
export class EmpleadoTiempoCompleto implements Empleado {
  obtenerDetalle(): string {
    return 'Empleado a Tiempo Completo';
  }
}