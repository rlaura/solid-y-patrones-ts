import { Empleado } from "../interfaces/empleado";

// Implementación de dos tipos de empleados
export class EmpleadoTiempoParcial implements Empleado {
  obtenerDetalle(): string {
    return "Empleado a Tiempo Parcial";
  }
}
