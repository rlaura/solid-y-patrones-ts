import { EmpleadoTiempoParcial } from "../implementacionEmpleados/empleadoTiempoParcial";
import { Empleado } from "../interfaces/empleado";
import { FabricaEmpleados } from "../interfaces/fabrica";

export class FabricaEmpleadoTiempoParcial implements FabricaEmpleados {
  crearEmpleado(): Empleado {
    return new EmpleadoTiempoParcial();
  }
}
