import { EmpleadoTiempoCompleto } from "../implementacionEmpleados/empleadoTiempoCompleto";
import { Empleado } from "../interfaces/empleado";
import { FabricaEmpleados } from "../interfaces/fabrica";

// Implementación de dos fábricas concretas de empleados
export class FabricaEmpleadoTiempoCompleto implements FabricaEmpleados {
  crearEmpleado(): Empleado {
    return new EmpleadoTiempoCompleto();
  }
}