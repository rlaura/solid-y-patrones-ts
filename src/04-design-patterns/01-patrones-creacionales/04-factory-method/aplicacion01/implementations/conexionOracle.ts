import { IConexion } from "../interfaces/iconexion";

export class ConexionOracle implements IConexion {
  private host: string;
  private puerto: string;
  private usuario: string;
  private contrasenia: string;

  constructor() {
    this.host = "localhost";
		this.puerto = "1521";
		this.usuario = "admin";
		this.contrasenia = "123";
  }

  conectar(): void {
    console.log("se conecto a Oracle");
  }

  desconectar = (): void => {
    console.log("se desconecto de Oracle");
  };
}