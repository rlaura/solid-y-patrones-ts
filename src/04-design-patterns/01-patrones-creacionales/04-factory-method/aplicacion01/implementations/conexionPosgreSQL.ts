import { IConexion } from "../interfaces/iconexion";

export class ConexionPostgreSQL implements IConexion {
  private host: string;
  private puerto: string;
  private usuario: string;
  private contrasenia: string;

  constructor() {
    this.host = "localhost";
		this.puerto = "5433";
		this.usuario = "postgres";
		this.contrasenia = "123";
  }

  conectar(): void {
    console.log("se conecto a PostgreSQL");
  }

  desconectar = (): void => {
    console.log("se desconecto de PostgreSQL");
  };
}
