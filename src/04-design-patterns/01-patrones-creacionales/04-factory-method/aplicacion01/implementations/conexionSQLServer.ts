import { IConexion } from "../interfaces/iconexion";

export class ConexionSQLServer implements IConexion {
  private host: string;
  private puerto: string;
  private usuario: string;
  private contrasenia: string;

  constructor() {
    this.host = "localhost";
    this.puerto = "1433";
    this.usuario = "postgres";
    this.contrasenia = "123";
  }

  conectar(): void {
    console.log("se conecto a SQLServer");
  }

  desconectar = (): void => {
    console.log("se desconecto de SQLServer");
  };
}
