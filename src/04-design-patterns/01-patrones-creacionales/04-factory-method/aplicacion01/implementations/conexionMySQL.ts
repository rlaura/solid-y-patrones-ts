import { IConexion } from "../interfaces/iconexion";

export class ConexionMySQL implements IConexion {
  private host: string;
  private puerto: string;
  private usuario: string;
  private contrasenia: string;

  constructor() {
    this.host = "localhost";
    this.puerto = "3306";
    this.usuario = "root";
    this.contrasenia = "12345";
  }

  conectar(): void {
    console.log("se conecto a MySQL");
  }

  desconectar = (): void => {
    console.log("se desconecto de MySQL");
  };
}
