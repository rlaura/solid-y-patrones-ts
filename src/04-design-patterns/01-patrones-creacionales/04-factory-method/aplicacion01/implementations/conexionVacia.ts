import { IConexion } from "../interfaces/iconexion";

export class ConexionVacia implements IConexion {
  conectar(): void {
    console.log("NO SE ESPECIFICÓ PROVEEDOR - PROVEEDOR NO ENCONTRADO");
  }

  desconectar = (): void => {
    console.log("NO SE ESPECIFICÓ PROVEEDOR - PROVEEDOR NO ENCONTRADO");
  };
}
