import { ConexionFabrica } from "./conexionFabrica";

function main() {
  //Intanciamos la fabrica
  const fabrica: ConexionFabrica = new ConexionFabrica();

  //ahora creamos la instacia de conexion a MySQL
  const conexion1 = fabrica.getConexiones("MySQL");
  conexion1.conectar();
  conexion1.desconectar();

  console.log("===================================================");

  //ahora creamos la instacia de conexion a PostgreSQL
  const conexion2 = fabrica.getConexiones("PostgreSQL");
  conexion2.conectar();
  conexion2.desconectar();

  console.log("===================================================");
  //ahora creamos la instacia de conexion a mariaDB
  const conexion3 = fabrica.getConexiones("mariaDB");
  conexion3.conectar();
  conexion3.desconectar();
}
main();
