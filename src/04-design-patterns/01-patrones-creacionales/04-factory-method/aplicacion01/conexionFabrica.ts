import { ConexionMySQL } from "./implementations/conexionMySQL";
import { ConexionOracle } from "./implementations/conexionOracle";
import { ConexionPostgreSQL } from "./implementations/conexionPosgreSQL";
import { ConexionSQLServer } from "./implementations/conexionSQLServer";
import { ConexionVacia } from "./implementations/conexionVacia";
import { IConexion } from "./interfaces/iconexion";
//TODO: Las fabricas devuelven instancias
export class ConexionFabrica {
  //creamos una funcion que devuelva una instancia conexion
  //ya que esa es la funcion de la fabrica devolver instancias
  getConexiones(motorBD: string): IConexion {
    if (motorBD === null) {
      return new ConexionVacia();
    }
    if (equalsIgnoreCase(motorBD, "MySQL")) {
      return new ConexionMySQL();
    }
    if (equalsIgnoreCase(motorBD, "PostgreSQL")) {
      return new ConexionPostgreSQL();
    }
    if (equalsIgnoreCase(motorBD, "Oracle")) {
      return new ConexionOracle();
    }
    if (equalsIgnoreCase(motorBD, "SQLServer")) {
      return new ConexionSQLServer();
    }

    return new ConexionVacia();
  }
}

const equalsIgnoreCase = (str1: string, str2: string): boolean => {
  return str1.toLowerCase() === str2.toLowerCase();
};
