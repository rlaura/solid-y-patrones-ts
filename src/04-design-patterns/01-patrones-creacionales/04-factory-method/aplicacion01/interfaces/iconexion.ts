export interface IConexion {
  //puede ser una funcion normal
  conectar(): void;
  //puede ser una arrow function tambien
  desconectar: () => void;
}
