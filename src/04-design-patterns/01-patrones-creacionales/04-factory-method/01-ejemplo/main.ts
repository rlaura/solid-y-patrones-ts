import { ConcreteCreator1, ConcreteCreator2 } from "./concreteCreator";
import { Creator } from "./interfaces/creator";

/**
 * El código del cliente funciona con una instancia de un creador concreto, aunque a través de
 * su interfaz base. Mientras el cliente siga trabajando con el creador a través de
 * la interfaz base, puedes pasarle la subclase de cualquier creador.
 */
function clientCode(creator: Creator) {
  // ...
  console.log(
    "Cliente: No conozco la clase del creador, pero aún así funciona."
  );
  console.log(creator.someOperation());
  // ...
}

function main() {
  /**
   * La Aplicación elige el tipo de creador dependiendo de la configuración o
   *ambiente.
   */
  console.log("Aplicación: Lanzada con ConcreteCreator1.");
  clientCode(new ConcreteCreator1());
  console.log("");

  console.log("Aplicación: Lanzada con ConcreteCreator2.");
  clientCode(new ConcreteCreator2());
}
main();
