import { Product } from "./interfaces/product";

/**
 * ConcreteProducts proporciona varias implementaciones de la interfaz del Producto.
 */
export class ConcreteProduct1 implements Product {
  public operation(): string {
    return "{Resultado del ProductoConcreto1}";
  }
}

export class ConcreteProduct2 implements Product {
  public operation(): string {
    return "{Resultado del ProductoConcreto2}";
  }
}
