import { ConcreteProduct1, ConcreteProduct2 } from "./concreteProduct";
import { Creator } from "./interfaces/creator";
import { Product } from "./interfaces/product";

/**
 * ConcreteCreators anula el método de fábrica para cambiar el
 * tipo de producto resultante.
 */
export class ConcreteCreator1 extends Creator {
  /**
   * Tenga en cuenta que la firma del método todavía utiliza el producto abstracto.
   * tipo, aunque el producto concreto realmente se devuelva del
   * método. De esta manera el Creador puede mantenerse independiente del producto concreto.
   *clases.
   */
  public factoryMethod(): Product {
    return new ConcreteProduct1();
  }
}

export class ConcreteCreator2 extends Creator {
  public factoryMethod(): Product {
    return new ConcreteProduct2();
  }
}
