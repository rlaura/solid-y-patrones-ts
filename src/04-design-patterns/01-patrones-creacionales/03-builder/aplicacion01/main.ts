import { User } from "./user";
import { UserBuilder } from "./user-builder";

function main() {
  const user: User = UserBuilder.user()
    .withFirstName("Dave")
    .withLastName("Johnson")
    .withAge(55)
    .liveInStreet("tu casa")
    .build();

  user.print();
  console.log("data: =>", user.print());
}

main();
