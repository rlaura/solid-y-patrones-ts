import { Builder } from "./builder";
import { Product1 } from "./product1";

/**
 * Las clases de Concrete Builder siguen la interfaz de Builder y proporcionan
 * implementaciones específicas de los pasos de construcción. Su programa puede tener varios
 * variaciones de Builders, implementadas de manera diferente.
 */
export class ConcreteBuilder1 implements Builder {
  private product!: Product1;

  /**
   * A fresh builder instance should contain a blank product object, which is
   * used in further assembly.
   */
  constructor() {
    this.reset();
  }

  public reset(): void {
    this.product = new Product1();
  }

  /**
   * Todos los pasos de producción funcionan con la misma instancia de producto.
   */
  public producePartA(): void {
    this.product.parts.push("PartA1");
  }

  public producePartB(): void {
    this.product.parts.push("PartB1");
  }

  public producePartC(): void {
    this.product.parts.push("PartC1");
  }

  /**
   * Se supone que los constructores de hormigón deben proporcionar sus propios métodos para
   * recuperar resultados. Esto se debe a que varios tipos de constructores pueden crear
   * productos completamente diferentes que no siguen la misma interfaz.
   * Por lo tanto, dichos métodos no se pueden declarar en la interfaz base del Constructor.
   * (al menos en un lenguaje de programación de tipo estático).
   *
   * Generalmente, después de devolver el resultado final al cliente, se crea una instancia del constructor.
   *Se espera que esté listo para comenzar a producir otro producto. Es por eso
   * es una práctica habitual llamar al método de reinicio al final del
   * Cuerpo del método `getProduct`. Sin embargo, este comportamiento no es obligatorio y
   * puedes hacer que tus constructores esperen una llamada de reinicio explícita desde el
   * código de cliente antes de deshacerse del resultado anterior.
   */
  public getProduct(): Product1 {
    const result = this.product;
    this.reset();
    return result;
  }
}
