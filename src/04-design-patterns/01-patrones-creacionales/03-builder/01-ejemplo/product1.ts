/**
 * Tiene sentido utilizar el patrón Builder sólo cuando sus productos sean bastante
 * complejo y requiere una configuración extensa.
 *
 * A diferencia de otros patrones creacionales, diferentes constructores concretos pueden producir
 * productos no relacionados. En otras palabras, es posible que los resultados de varios constructores no
 * seguir siempre la misma interfaz.
 */
export class Product1 {
  public parts: string[] = [];

  public listParts(): void {
    console.log(`Product parts: ${this.parts.join(", ")}\n`);
  }
}
