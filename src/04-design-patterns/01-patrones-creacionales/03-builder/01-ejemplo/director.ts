import { Builder } from "./builder";

/**
 * El Director sólo es responsable de ejecutar los pasos de construcción en un
 *secuencia particular. Es útil cuando se producen productos de acuerdo con un
 * orden o configuración específica. Estrictamente hablando, la clase Directora es
 *opcional, ya que el cliente puede controlar directamente a los constructores.
 */
export class Director {
  private builder!: Builder;

  /**
   * El Director trabaja con cualquier instancia de constructor que pase el código del cliente.
   * a ello. De esta manera, el código del cliente puede alterar el tipo final del nuevo
   * producto ensamblado.
   */
  public setBuilder(builder: Builder): void {
    this.builder = builder;
  }

  /**
   * El Director puede construir varias variaciones de productos utilizando el mismo
   * pasos de construcción.
   */
  public buildMinimalViableProduct(): void {
    this.builder.producePartA();
  }

  public buildFullFeaturedProduct(): void {
    this.builder.producePartA();
    this.builder.producePartB();
    this.builder.producePartC();
  }
}
