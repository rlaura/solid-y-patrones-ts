import { ConcreteBuilder1 } from "./concreteBuilder1";
import { Director } from "./director";

/**
 * El código del cliente crea un objeto constructor, lo pasa al director y luego
 * inicia el proceso de construcción. El resultado final se obtiene del
 * objeto constructor.
 */
function clientCode(director: Director) {
  const builder = new ConcreteBuilder1();
  director.setBuilder(builder);

  console.log("Producto básico estándar:");
  director.buildMinimalViableProduct();
  builder.getProduct().listParts();

  console.log("Producto estándar con todas las funciones:");
  director.buildFullFeaturedProduct();
  builder.getProduct().listParts();

  // Recuerde, el patrón Builder se puede utilizar sin una clase Director.
  console.log("Producto personalizado:");
  builder.producePartA();
  builder.producePartC();
  builder.getProduct().listParts();
}

function main() {
  const director = new Director();
  clientCode(director);
}

main();
