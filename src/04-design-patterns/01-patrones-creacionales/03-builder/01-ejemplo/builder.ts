/**
 * La interfaz Builder especifica métodos para crear las diferentes partes de
 * los objetos del Producto.
 */
export interface Builder {
  producePartA(): void;
  producePartB(): void;
  producePartC(): void;
}
