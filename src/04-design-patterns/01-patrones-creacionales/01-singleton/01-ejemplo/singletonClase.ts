/**
 * La clase Singleton define el método `getInstance` que permite a los clientes acceder
 * la instancia única única.
 */
export class Singleton {
  private static instance: Singleton;

  /**
   * El constructor del Singleton siempre debe ser privado para evitar accesos directos.
   * llamadas de construcción con el operador `nuevo`.
   */
  private constructor() {}

  /**
   * El método estático que controla el acceso a la instancia singleton.
   *
   * Esta implementación le permite subclasificar la clase Singleton manteniendo
   * solo una instancia de cada subclase.
   */
  public static getInstance(): Singleton {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton();
      console.log("Creando una instancia única ahora.");
    }
    console.log("La Instancia única ya fue creada");
    return Singleton.instance;
  }

  /**
   * Finalmente, cualquier singleton debe definir alguna lógica de negocios, que puede ser
   * ejecutado en su instancia.
   */
  public someBusinessLogic() {
    // Logica de negocios
    console.log(
      "Alguna logica de negocio que se pueda ejecutar en esta instancia"
    );
  }
}
