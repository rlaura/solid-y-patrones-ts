import { Singleton } from "./singletonClase";

/**
 * El código de cliente.
 */
function clientCode() {
  const s1 = Singleton.getInstance();
  const s2 = Singleton.getInstance();

  if (s1 === s2) {
    console.log(
      "Singleton funciona, ambas variables contienen la misma instancia."
    );
  } else {
    console.log(
      "Singleton falló, las variables contienen instancias diferentes."
    );
  }

  //TODO: comprobando que solo existe una sola instancia del singleton
  for (let i = 0; i < 10; i++) {
    const s3 = Singleton.getInstance();
  }
}

clientCode();
