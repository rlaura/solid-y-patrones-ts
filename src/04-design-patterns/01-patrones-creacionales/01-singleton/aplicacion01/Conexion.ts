export class Conexion {
  //Declaración
  private static instancia: Conexion;
  //Para evitar instancia mediante operador "new"
  private constructor() {}

  //Para obtener la instancia unicamente por este metodo
	//Notese la palabra reservada "static" hace posible el acceso mediante Clase.metodo
  public static getInstancia(): Conexion {
    if (!Conexion.instancia) {
      Conexion.instancia = new Conexion();
      console.log("Creando una instancia CONEXION única ahora.");
    }
    console.log("La Instancia única DE CONEXION ya fue creada");
    return Conexion.instancia;
  }

  //Método de prueba
	public conectar() {
		console.log("Me conecté a la BD");
	}
	
	//Método de prueba
	public  desconectar() {
		console.log("Me desconecté de la BD");
	}
  
}
