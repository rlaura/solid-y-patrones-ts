import { Conexion } from "./Conexion";

/**
 * El código de cliente.
 */
function main() {
  const conexion = Conexion.getInstancia();
  conexion.conectar();
  conexion.desconectar();

  const rpta = conexion instanceof Conexion;
  console.log(rpta);

  //TODO: comprobando que solo existe una sola instancia del singleton
  for (let i = 0; i < 10; i++) {
    const c2 = Conexion.getInstancia();
  }
}

main();
