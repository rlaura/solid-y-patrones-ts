import { AdaptadorTransaccion } from "./adaptadorTransaccion";
import { SistemaDePagoExterno } from "./implementaciones/sistemaDePagoExterno";
import { SistemaInterno } from "./implementaciones/sistemaInterno";
import { IDetallesTransaccionInternos } from "./interfaces/detallesTransaccionInternos";
/**
 * TODO: Imaginemos un escenario en el que estás trabajando en una aplicación empresarial
 * que utiliza un sistema de pago externo que espera datos en un formato específico.
 * Sin embargo, tu sistema interno tiene un formato diferente para los detalles de la transacción.
 * Aquí puedes aplicar el patrón Adapter para hacer que ambos sistemas funcionen juntos.
 */
function main() {
  // Uso del Adapter en el entorno
  const sistemaInterno = new SistemaInterno();
  const detallesInternos: IDetallesTransaccionInternos = {
    cliente: "ClienteA",
    monto: 100,
    descripcion: "Compra en línea",
  };
  sistemaInterno.realizarTransaccion(detallesInternos);

  const adapter = new AdaptadorTransaccion(detallesInternos);

  const sistemaExterno = new SistemaDePagoExterno();
  sistemaExterno.procesarPago(adapter);
  /**
   * -SistemaInterno: representa el sistema interno que realiza transacciones con un formato específico
   *                  de detalles de transacción (DetallesTransaccionInternos).
   * -SistemaDePagoExterno: representa un sistema de pago externo que espera detalles de transacción
   *                        en un formato diferente (DetallesTransaccionExternos).
   * -AdaptadorTransaccion: es un Adapter que adapta los detalles de la transacción internos al formato
   *                        requerido por el sistema de pago externo.
   */
}
main();
