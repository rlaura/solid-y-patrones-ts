import { IDetallesTransaccionExternos } from "../interfaces/detallesTransaccionExternos";

// Sistema de pago externo que espera datos en un formato diferente
export class SistemaDePagoExterno {
  procesarPago(detalles: IDetallesTransaccionExternos): void {
    console.log(
      `Procesando pago externo para ${detalles.customerName} por ${detalles.amount} USD.`
    );
  }
}
