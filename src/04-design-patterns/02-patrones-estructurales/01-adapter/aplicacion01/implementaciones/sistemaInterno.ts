import { IDetallesTransaccionInternos } from "../interfaces/detallesTransaccionInternos";

// Sistema interno que trabaja con un formato específico de detalles de transacción
export class SistemaInterno {
  realizarTransaccion(detalles: IDetallesTransaccionInternos): void {
    console.log(`Realizando transacción interna para ${detalles.cliente} por ${detalles.monto} USD.`);
  }
}