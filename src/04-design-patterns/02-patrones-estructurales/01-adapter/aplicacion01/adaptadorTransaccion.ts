import { IDetallesTransaccionExternos } from "./interfaces/detallesTransaccionExternos";
import { IDetallesTransaccionInternos } from "./interfaces/detallesTransaccionInternos";

// Adapter que adapta los detalles de la transacción internos al formato requerido por el sistema de pago externo
export class AdaptadorTransaccion implements IDetallesTransaccionExternos {
  private detallesInternos: IDetallesTransaccionInternos;

  constructor(detallesInternos: IDetallesTransaccionInternos) {
    this.detallesInternos = detallesInternos;
  }

  get customerName(): string {
    return this.detallesInternos.cliente;
  }

  get amount(): number {
    return this.detallesInternos.monto;
  }

  get description(): string {
    return this.detallesInternos.descripcion;
  }
}
