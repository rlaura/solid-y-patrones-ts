// Interfaz existente en el sistema interno
export interface IDetallesTransaccionInternos {
  cliente: string;
  monto: number;
  descripcion: string;
}
