// Interfaz requerida por el sistema de pago externo
export interface IDetallesTransaccionExternos {
  customerName: string;
  amount: number;
  description: string;
}