import { Adaptee } from "./adaptee";
import { Target } from "./target";

/**
 * El Adapter hace que la interfaz del Adaptee sea compatible con la interfaz del Target.
 */
export class Adapter extends Target {
  private adaptee: Adaptee;

  constructor(adaptee: Adaptee) {
      super();
      this.adaptee = adaptee;
  }

  public request(): string {
      const result = this.adaptee.specificRequest().split('').reverse().join('');
      return `Adapter: ${result}`;
  }
}