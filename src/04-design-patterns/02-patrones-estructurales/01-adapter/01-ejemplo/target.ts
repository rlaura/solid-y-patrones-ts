/**
 * El Target define la interfaz específica del dominio utilizada por el código del cliente.
 */
export class Target {
  public request(): string {
      return 'Target: Comportamiento predeterminado del objetivo.';
  }
}