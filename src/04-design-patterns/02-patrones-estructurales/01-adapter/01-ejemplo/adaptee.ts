/**
 * El Adaptee contiene algún comportamiento útil, pero su interfaz es incompatible
 * con el código del cliente existente. El Adaptee necesita alguna adaptación antes
 * de que el código del cliente pueda utilizarlo.
 */
export class Adaptee {
  public specificRequest(): string {
      return '.eetpadA eht fo roivaheb laicepS';
  }
}
