import { Adaptee } from "./adaptee";
import { Adapter } from "./adapter";
import { Target } from "./target";

/**
 * El código del cliente admite todas las clases que siguen la interfaz del Target.
 */
function clientCode(target: Target) {
  console.log(target.request());
}

function main() {
  console.log('Cliente: Puedo trabajar perfectamente con objetos Target:');
  const target = new Target();
  clientCode(target);

  console.log('');

  const adaptee = new Adaptee();
  console.log('Cliente: La clase Adaptee tiene una interfaz extraña. Mira, no la entiendo:');
  console.log(`Adaptee: ${adaptee.specificRequest()}`);

  console.log('');

  console.log('Cliente: Pero puedo trabajar con ella a través del Adapter:');
  const adapter = new Adapter(adaptee);
  clientCode(adapter);
}

main();