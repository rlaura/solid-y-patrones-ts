import { AdaptadorServicio } from "./adaptadorServicio";
import { NuevoServicio } from "./implementacion/nuevoServicio";
import { ServicioAntiguo } from "./implementacion/servicioAntiguo";
/**
 * TODO: Supongamos que tienes un servicio existente que proporciona
 * datos en un formato específico, pero necesitas adaptar esos datos
 * para que sean compatibles con un nuevo servicio que espera un formato
 * diferente.
 */
function main() {
  // Uso del Adapter en el entorno
  const servicioAntiguo = new ServicioAntiguo();
  const adapter = new AdaptadorServicio(servicioAntiguo);

  const nuevoServicio = new NuevoServicio();
  nuevoServicio.mostrarDatos(adapter);
  /**
   * TODO: 
   * -ServicioAntiguo: representa un servicio existente que proporciona datos en un formato específico (DatosAntiguos).
   * -NuevoServicio: representa un nuevo servicio que espera datos en un formato diferente (DatosNuevos).
   * -AdaptadorServicio: es un Adapter que adapta los datos del servicio antiguo al formato requerido por el 
   *                     nuevo servicio.
   */
}
main();
