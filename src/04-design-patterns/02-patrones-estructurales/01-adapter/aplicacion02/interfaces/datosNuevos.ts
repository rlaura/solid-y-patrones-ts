// Interfaz requerida por el nuevo servicio
export interface IDatosNuevos {
  firstName: string;
  age: number;
}