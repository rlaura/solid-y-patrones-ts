// Interfaz existente en el servicio antiguo
export interface IDatosAntiguos {
  nombre: string;
  edad: number;
}