import { ServicioAntiguo } from "./implementacion/servicioAntiguo";
import { IDatosNuevos } from "./interfaces/DatosNuevos";

// Adapter que adapta los datos del servicio antiguo al formato requerido por el nuevo servicio
export class AdaptadorServicio implements IDatosNuevos {
  private servicioAntiguo: ServicioAntiguo;

  constructor(servicioAntiguo: ServicioAntiguo) {
    this.servicioAntiguo = servicioAntiguo;
  }

  get firstName(): string {
    return this.servicioAntiguo.obtenerDatos().nombre;
  }

  get age(): number {
    return this.servicioAntiguo.obtenerDatos().edad;
  }
}
