import { IDatosNuevos } from "../interfaces/DatosNuevos";

// Nuevo servicio que espera datos en un formato diferente
export class NuevoServicio {
  mostrarDatos(datos: IDatosNuevos): void {
    console.log(`Nombre: ${datos.firstName}, Edad: ${datos.age}`);
  }
}
