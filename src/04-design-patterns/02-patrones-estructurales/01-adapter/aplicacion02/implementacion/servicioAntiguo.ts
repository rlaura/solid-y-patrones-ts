import { IDatosAntiguos } from "../interfaces/DatosAntiguos";

// Servicio antiguo que proporciona datos en un formato específico
export class ServicioAntiguo {
  obtenerDatos(): IDatosAntiguos {
    return {
      nombre: 'Juan',
      edad: 25,
    };
  }
}