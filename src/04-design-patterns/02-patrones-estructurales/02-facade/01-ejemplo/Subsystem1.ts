/**
 * El Subsistema puede aceptar solicitudes tanto del facade como directamente del cliente.
 * En cualquier caso, para el Subsistema, el Facade es solo otro cliente, y no es
 * parte del Subsistema.
 */
export class Subsystem1 {
  public operation1(): string {
    return "Subsistema1: ¡Listo!\n";
  }

  // ...

  public operationN(): string {
    return "Subsistema1: ¡Vamos!\n";
  }
}
