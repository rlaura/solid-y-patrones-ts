/**
 * Algunos facades pueden trabajar con múltiples subsistemas al mismo tiempo.
 */
export class Subsystem2 {
  public operation1(): string {
    return "Subsistema2: ¡Prepárate!\n";
  }

  // ...

  public operationZ(): string {
    return "Subsistema2: ¡Fuego!";
  }
}
