import { Facade } from "./Facade";
import { Subsystem1 } from "./Subsystem1";
import { Subsystem2 } from "./Subsystem2";

/**
 * El código del cliente trabaja con subsistemas complejos a través de una interfaz simple
 * proporcionada por el Facade. Cuando un facade gestiona el ciclo de vida del subsistema,
 * el cliente puede que ni siquiera conozca la existencia del subsistema. Este
 * enfoque te permite mantener la complejidad bajo control.
 */
function clientCode(facade: Facade) {
  // ...

  console.log(facade.operation());

  // ...
}

function main() {
  /**
   * El código del cliente puede tener algunos objetos del subsistema ya creados. En
   * este caso, podría valer la pena inicializar el Facade con estos objetos
   * en lugar de permitir que el Facade cree nuevas instancias.
   */
  const subsystem1 = new Subsystem1();
  const subsystem2 = new Subsystem2();
  const facade = new Facade(subsystem1, subsystem2);
  clientCode(facade);
}
main();