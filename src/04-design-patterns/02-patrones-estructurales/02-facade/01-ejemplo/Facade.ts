import { Subsystem1 } from "./Subsystem1";
import { Subsystem2 } from "./Subsystem2";

/**
 * La clase Facade proporciona una interfaz simple para la lógica compleja de uno o
 * varios subsistemas. El Facade delega las solicitudes del cliente a los
 * objetos apropiados dentro del subsistema. El Facade también es responsable de
 * gestionar su ciclo de vida. Todo esto protege al cliente de la complejidad no deseada
 * del subsistema.
 */
export class Facade {
  protected subsystem1: Subsystem1;

  protected subsystem2: Subsystem2;

  /**
   * Dependiendo de las necesidades de tu aplicación, puedes proporcionar al Facade
   * objetos de subsistemas existentes o forzar al Facade a crearlos por su cuenta.
   */
  constructor(subsystem1?: Subsystem1, subsystem2?: Subsystem2) {
    this.subsystem1 = subsystem1 || new Subsystem1();
    this.subsystem2 = subsystem2 || new Subsystem2();
  }

  /**
   * Los métodos del Facade son accesos directos convenientes a la funcionalidad sofisticada
   * de los subsistemas. Sin embargo, los clientes solo acceden a una fracción
   * de las capacidades de un subsistema.
   */
  public operation(): string {
    let result = "Facade inicializa los subsistemas:\n";
    result += this.subsystem1.operation1();
    result += this.subsystem2.operation1();
    result += "Facade ordena a los subsistemas realizar la acción:\n";
    result += this.subsystem1.operationN();
    result += this.subsystem2.operationZ();

    return result;
  }
}
