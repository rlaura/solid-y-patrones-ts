import { AccountingSubsystem } from "./AccountingSubsystem";
import { InventorySubsystem } from "./InventorySubsystem";
import { ShippingSubsystem } from "./ShippingSubsystem";

// Facade
export class EnterpriseFacade {
  private inventorySubsystem: InventorySubsystem;
  private accountingSubsystem: AccountingSubsystem;
  private shippingSubsystem: ShippingSubsystem;

  constructor() {
    this.inventorySubsystem = new InventorySubsystem();
    this.accountingSubsystem = new AccountingSubsystem();
    this.shippingSubsystem = new ShippingSubsystem();
  }

  public processOrder(product: string, quantity: number): void {
    const availableQuantity = this.inventorySubsystem.checkInventory(product);
    if (availableQuantity >= quantity) {
      console.log(`Producto ${product} disponible en inventario.`);
      const revenue = this.accountingSubsystem.calculateRevenue();
      console.log(`Ingresos calculados: $${revenue}.`);
      this.shippingSubsystem.shipProduct(product, quantity);
    } else {
      console.log(`Producto ${product} no disponible en inventario.`);
    }
  }
}
