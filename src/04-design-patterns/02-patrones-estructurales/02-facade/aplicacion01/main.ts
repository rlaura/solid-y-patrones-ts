import { EnterpriseFacade } from "./EnterpriseFacade";
/**
 * Supongamos que tienes un sistema empresarial con múltiples subsistemas complejos, 
 * como InventorySubsystem, AccountingSubsystem y ShippingSubsystem. Puedes crear un 
 * Facade para proporcionar una interfaz unificada y simplificada para interactuar con estos subsistemas.
 */
function main() {
  // Uso del Facade
  console.log("==============FACADE EJEMPLO APLICATIVO 1==================");
  const facade = new EnterpriseFacade();
  facade.processOrder("Laptop", 5);
}
main();
