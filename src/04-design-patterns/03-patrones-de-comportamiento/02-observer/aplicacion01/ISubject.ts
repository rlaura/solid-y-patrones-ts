import { IObserver } from "./IObserver";

// Interfaz del Sujeto
export interface ISubject {
  attach(observer: IObserver): void;
  detach(observer: IObserver): void;
  notify(data: any): void;
}