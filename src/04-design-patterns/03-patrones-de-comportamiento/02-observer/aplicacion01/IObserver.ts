// Interfaz del Observador
export interface IObserver {
  update(data: any): void;
}