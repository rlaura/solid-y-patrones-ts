import { IObserver } from "./IObserver";
import { ISubject } from "./ISubject";

// Sujeto Concreto
export class NewsAgency implements ISubject {
  private observers: IObserver[] = [];

  attach(observer: IObserver): void {
    this.observers.push(observer);
  }

  detach(observer: IObserver): void {
    const index = this.observers.indexOf(observer);
    if (index !== -1) {
      this.observers.splice(index, 1);
    }
  }

  notify(data: any): void {
    this.observers.forEach((observer) => observer.update(data));
  }

  publishNews(news: string): void {
    console.log(`Publicando noticias: ${news}`);
    this.notify(news);
  }
}
