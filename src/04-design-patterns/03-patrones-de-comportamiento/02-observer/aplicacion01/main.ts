import { NewsAgency } from "./NewsAgency";
import { NewsSubscriber } from "./NewsSubscriber";
/**
 * NewsAgency es el Sujeto Concreto que tiene una lista de Observadores y métodos para adjuntar, desvincular y notificar a los Observadores.
 * NewsSubscriber es un Observador Concreto que implementa la interfaz Observer.
 * NewsAgency publica noticias utilizando el método publishNews, y notifica a 
 *            todos los observadores cuando se publica una noticia utilizando el método notify.
 * 
 * Los observadores reciben las noticias y realizan acciones específicas, en este caso, simplemente imprimiendo el mensaje recibido.
 */
function main() {
  // Uso del patrón Observer en un entorno empresarial simulado
  console.log("================PATRON OBSERVER APLICATIVO=================");
  const newsAgency = new NewsAgency();

  const subscriber1 = new NewsSubscriber("Suscriptor 1");
  const subscriber2 = new NewsSubscriber("Suscriptor 2");

  newsAgency.attach(subscriber1);
  newsAgency.attach(subscriber2);

  newsAgency.publishNews("¡Nuevas actualizaciones en el mercado de valores!");

  newsAgency.detach(subscriber2);

  newsAgency.publishNews("¡Anuncio importante sobre la empresa!");
}
main();
