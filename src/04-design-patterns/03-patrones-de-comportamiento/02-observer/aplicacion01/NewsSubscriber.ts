import { IObserver } from "./IObserver";

// Observador Concreto
export class NewsSubscriber implements IObserver {
  private name: string;

  constructor(name: string) {
      this.name = name;
  }

  update(data: any): void {
      console.log(`${this.name} recibió noticias: ${data}`);
  }
}