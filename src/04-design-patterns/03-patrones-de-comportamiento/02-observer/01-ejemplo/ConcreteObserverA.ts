import { ConcreteSubject } from "./ConcreteSubject";
import { IObserver } from "./IObserver";
import { ISubject } from "./ISubject";

/**
 * Los Observadores Concretos reaccionan a las actualizaciones emitidas por el Sujeto al que estaban
 * adjuntos.
 */
export class ConcreteObserverA implements IObserver {
  public update(subject: ISubject): void {
      if (subject instanceof ConcreteSubject && subject.state < 3) {
          console.log('ObservadorConcretoA: Reactuó ante el evento.');
      }
  }
}