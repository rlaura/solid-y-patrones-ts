import { ConcreteObserverA } from "./ConcreteObserverA";
import { ConcreteObserverB } from "./ConcreteObserverB";
import { ConcreteSubject } from "./ConcreteSubject";
/**
 * Observer es un patrón de diseño de comportamiento que permite a un objeto notificar a otros objetos 
 * sobre cambios en su estado.
 */
function main() {
  /**
   * El código del cliente.
   */
  console.log("================PATRON OBSERVER EJEMPLO REFACTORING GURU=================");
  const subject = new ConcreteSubject();

  const observer1 = new ConcreteObserverA();
  subject.attach(observer1);

  const observer2 = new ConcreteObserverB();
  subject.attach(observer2);

  subject.someBusinessLogic();
  subject.someBusinessLogic();

  subject.detach(observer2);

  subject.someBusinessLogic();
}
main();
