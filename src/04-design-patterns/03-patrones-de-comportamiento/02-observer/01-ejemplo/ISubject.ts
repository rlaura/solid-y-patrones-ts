import { IObserver } from "./IObserver";

/**
 * La interfaz Subject declara un conjunto de métodos para gestionar suscriptores.
 */
export interface ISubject {
  // Adjunta un observador al sujeto.
  attach(observer: IObserver): void;

  // Desvincula un observador del sujeto.
  detach(observer: IObserver): void;

  // Notifica a todos los observadores sobre un evento.
  notify(): void;
}