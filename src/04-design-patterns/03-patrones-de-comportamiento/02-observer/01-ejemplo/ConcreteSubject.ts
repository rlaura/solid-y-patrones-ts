import { IObserver } from "./IObserver";
import { ISubject } from "./ISubject";

/**
 * El Sujeto posee un estado importante y notifica a los observadores cuando el estado cambia.
 */
export class ConcreteSubject implements ISubject {
  /**
   * @type {number} Por simplicidad, el estado del Sujeto, esencial
   * para todos los suscriptores, se almacena en esta variable.
   */
  public state!: number;

  /**
   * @type {Observer[]} Lista de suscriptores. En la vida real, la lista de
   * suscriptores puede almacenarse de forma más completa (categorizada por tipo de evento, etc.).
   */
  private observers: IObserver[] = [];

  /**
   * Los métodos de gestión de suscripciones.
   */
  public attach(observer: IObserver): void {
      const isExist = this.observers.includes(observer);
      if (isExist) {
          return console.log('Sujeto: El observador ya ha sido adjuntado.');
      }

      console.log('Sujeto: Se adjuntó un observador.');
      this.observers.push(observer);
  }

  public detach(observer: IObserver): void {
      const observerIndex = this.observers.indexOf(observer);
      if (observerIndex === -1) {
          return console.log('Sujeto: Observador inexistente.');
      }

      this.observers.splice(observerIndex, 1);
      console.log('Sujeto: Se desvinculó un observador.');
  }

  /**
   * Desencadena una actualización en cada suscriptor.
   */
  public notify(): void {
      console.log('Sujeto: Notificando a los observadores...');
      for (const observer of this.observers) {
          observer.update(this);
      }
  }

  /**
   * Por lo general, la lógica de suscripción es solo una fracción de lo que un Sujeto puede
   * hacer realmente. Los Sujetos comúnmente tienen alguna lógica de negocio importante, que
   * desencadena un método de notificación cuando algo importante está a punto de suceder (o después de eso).
   */
  public someBusinessLogic(): void {
      console.log('\nSujeto: Estoy haciendo algo importante.');
      this.state = Math.floor(Math.random() * (10 + 1));

      console.log(`Sujeto: Mi estado acaba de cambiar a: ${this.state}`);
      this.notify();
  }
}