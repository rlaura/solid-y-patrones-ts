import { ISubject } from "./ISubject";

/**
 * La interfaz Observer declara el método de actualización, utilizado por los sujetos.
 */
export interface IObserver {
  // Recibe una actualización del sujeto.
  update(subject: ISubject): void;
}