import { ConcreteSubject } from "./ConcreteSubject";
import { IObserver } from "./IObserver";
import { ISubject } from "./ISubject";

/**
 * Los Observadores Concretos reaccionan a las actualizaciones emitidas por el Sujeto al que estaban
 * adjuntos.
 */
export class ConcreteObserverB implements IObserver {
  public update(subject: ISubject): void {
      if (subject instanceof ConcreteSubject && (subject.state === 0 || subject.state >= 2)) {
          console.log('ObservadorConcretoB: Reactuó ante el evento.');
      }
  }
}