import { IObserver } from "./IObserver";
import { InventorySubject } from "./InventorySubject";

// Observador Concreto: InventoryObserver
export class InventoryObserver implements IObserver<number> {
  private inventorySubject: InventorySubject;

  constructor(inventorySubject: InventorySubject) {
    this.inventorySubject = inventorySubject;
  }

  update(data: number): void {
    console.log(`La cantidad de inventario ha sido actualizada a: ${data}`);
  }
}
