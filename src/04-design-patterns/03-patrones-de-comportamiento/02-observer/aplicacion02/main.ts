import { InventoryObserver } from "./InventoryObserver";
import { InventorySubject } from "./InventorySubject";
/**
 * InventorySubject: actúa como el Sujeto Concreto que representa el módulo de dominio de inventario.
 *                   Este sujeto mantiene un registro de la cantidad actual de inventario y notifica a
 *                   los observadores cuando esta cantidad cambia.
 * InventoryObserver: actúa como el Observador Concreto que está interesado en recibir notificaciones
 *                   sobre cambios en la cantidad de inventario.
 * Cuando se actualiza la cantidad de inventario utilizando el método updateQuantity del InventorySubject,
 *                   este notifica a todos los observadores registrados sobre el nuevo valor.
 * Los observadores, en este caso, simplemente imprimen un mensaje cuando reciben una actualización
 *                   sobre la cantidad de inventario.
 */
function main() {
  console.log(
    "================PATRON OBSERVER APLICADO A DDD================="
  );
  // Crear una instancia del sujeto de inventario
  const inventorySubject = new InventorySubject();

  // Crear observadores de inventario
  const observer1 = new InventoryObserver(inventorySubject);
  const observer2 = new InventoryObserver(inventorySubject);

  // Adjuntar observadores al sujeto de inventario
  inventorySubject.attach(observer1);
  inventorySubject.attach(observer2);

  // Actualizar la cantidad de inventario
  inventorySubject.updateQuantity(100);
  inventorySubject.updateQuantity(200);
  inventorySubject.updateQuantity(300);
}
main();
