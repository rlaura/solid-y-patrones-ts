import { IObserver } from "./IObserver";

// Interfaz del Sujeto
export interface ISubject<T> {
  attach(observer: IObserver<T>): void;
  detach(observer: IObserver<T>): void;
  notify(data: T): void;
}
