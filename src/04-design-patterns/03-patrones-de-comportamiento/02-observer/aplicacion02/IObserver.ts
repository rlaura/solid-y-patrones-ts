// Interfaz del Observador
export interface IObserver<T> {
  update(data: T): void;
}
