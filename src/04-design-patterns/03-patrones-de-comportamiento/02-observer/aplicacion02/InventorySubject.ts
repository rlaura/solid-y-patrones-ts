import { IObserver } from "./IObserver";
import { ISubject } from "./ISubject";

// Sujeto Concreto: InventorySubject
export class InventorySubject implements ISubject<number> {
  private observers: IObserver<number>[] = [];
  private currentQuantity: number = 0;

  attach(observer: IObserver<number>): void {
    this.observers.push(observer);
  }

  detach(observer: IObserver<number>): void {
    const index = this.observers.indexOf(observer);
    if (index !== -1) {
      this.observers.splice(index, 1);
    }
  }

  notify(data: number): void {
    this.observers.forEach((observer) => observer.update(data));
  }

  // Método para actualizar la cantidad de inventario y notificar a los observadores
  updateQuantity(newQuantity: number): void {
    this.currentQuantity = newQuantity;
    this.notify(this.currentQuantity);
  }

  getCurrentQuantity(): number {
    return this.currentQuantity;
  }
}
