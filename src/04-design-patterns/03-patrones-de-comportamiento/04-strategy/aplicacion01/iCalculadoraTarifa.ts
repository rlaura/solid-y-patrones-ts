// Definición de la interfaz para la estrategia de cálculo de tarifas
export interface ICalculadoraTarifa {
  calcularTarifa(monto: number): number;
}
