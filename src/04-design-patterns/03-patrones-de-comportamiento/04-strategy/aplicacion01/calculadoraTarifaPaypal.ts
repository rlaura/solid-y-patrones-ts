// Implementación de las estrategias concretas

import { ICalculadoraTarifa } from "./iCalculadoraTarifa";

export class CalculadoraTarifaPaypal implements ICalculadoraTarifa {
    // Lógica para calcular la tarifa de Paypal
    calcularTarifa(monto: number): number {
      // Supongamos que la tarifa es el 2% del monto más 0.3 dólares
      return monto * 0.02 + 0.3; 
    }
  }