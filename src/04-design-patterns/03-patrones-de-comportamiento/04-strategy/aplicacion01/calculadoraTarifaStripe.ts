import { ICalculadoraTarifa } from "./iCalculadoraTarifa";

export class CalculadoraTarifaStripe implements ICalculadoraTarifa {
    // Lógica para calcular la tarifa de Stripe
  calcularTarifa(monto: number): number {
    // Supongamos que la tarifa es el 3% del monto más 0.25 dólares
    return monto * 0.03 + 0.25; 
  }
}
