import { ICalculadoraTarifa } from "./iCalculadoraTarifa";

// Contexto que utiliza la estrategia de cálculo de tarifas
export class ProcesadorPago {
  private calculadoraTarifa: ICalculadoraTarifa;

  constructor(calculadoraTarifa: ICalculadoraTarifa) {
    this.calculadoraTarifa = calculadoraTarifa;
  }

  procesarPago(monto: number): void {
    const tarifa = this.calculadoraTarifa.calcularTarifa(monto);
    const montoTotal = monto + tarifa;
    console.log(`Monto a pagar: $${montoTotal} (Tarifa: $${tarifa})`);
    // Lógica adicional para procesar el pago...
  }
}
