import { CalculadoraTarifaPaypal } from "./calculadoraTarifaPaypal";
import { CalculadoraTarifaStripe } from "./calculadoraTarifaStripe";
import { ProcesadorPago } from "./procesadorPago";

/**
 * TODO:  Imaginemos un sistema de procesamiento de pagos en una aplicación financiera. 
 * Queremos aplicar diferentes estrategias de cálculo de tarifas de procesamiento de pagos. 
 * Cada estrategia representa un proveedor de servicios de pago con su propia lógica de tarifas. 
 */
function main() {
  // Crear instancias de las estrategias concretas
  const calculadoraPaypal = new CalculadoraTarifaPaypal();
  const calculadoraStripe = new CalculadoraTarifaStripe();

  // Crear instancias del contexto con diferentes estrategias
  const procesadorPagoPaypal = new ProcesadorPago(calculadoraPaypal);
  const procesadorPagoStripe = new ProcesadorPago(calculadoraStripe);
  console.log();
  console.log("Patron strategy");
  // Utilizar diferentes estrategias en tiempo de ejecución
  // Utiliza la estrategia de tarifas de Paypal
  procesadorPagoPaypal.procesarPago(100); 
  // Utiliza la estrategia de tarifas de Stripe
  procesadorPagoStripe.procesarPago(100); 
}
main();
