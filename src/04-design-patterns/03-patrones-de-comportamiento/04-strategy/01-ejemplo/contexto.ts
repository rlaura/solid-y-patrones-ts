import { IEstrategia } from "./iEstrategia";

/**
 * El Contexto define la interfaz de interés para los clientes.
 */
export class Contexto {
  /**
   * @type {Estrategia} El Contexto mantiene una referencia a uno de los objetos
   * de Estrategia. El Contexto no conoce la clase concreta de una estrategia.
   * Debería trabajar con todas las estrategias a través de la interfaz de Estrategia.
   */
  private estrategia: IEstrategia;

  /**
   * Usualmente, el Contexto acepta una estrategia a través del constructor, pero
   * también proporciona un método setter para cambiarla en tiempo de ejecución.
   */
  constructor(estrategia: IEstrategia) {
    this.estrategia = estrategia;
  }

  /**
   * Usualmente, el Contexto permite reemplazar un objeto de Estrategia en tiempo de ejecución.
   */
  public setEstrategia(estrategia: IEstrategia) {
    this.estrategia = estrategia;
  }

  /**
   * El Contexto delega parte del trabajo al objeto de Estrategia en lugar de
   * implementar múltiples versiones del algoritmo por sí mismo.
   */
  public hacerAlgoDeLogicaDeNegocio(): void {
    // ...

    console.log(
      "Contexto: Ordenando datos usando la estrategia (no estoy seguro de cómo lo hará)"
    );
    const resultado = this.estrategia.hacerAlgoritmo(["a", "b", "c", "d", "e"]);
    console.log(resultado.join(","));

    // ...
  }
}
