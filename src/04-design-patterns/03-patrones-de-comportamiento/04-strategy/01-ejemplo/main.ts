import { Contexto } from "./contexto";
import { EstrategiaConcretaA } from "./estrategiaA";
import { EstrategiaConcretaB } from "./estrategiaB";

/**
 * TODO:
 */
function main() {
  /**
   * El código del cliente elige una estrategia concreta y la pasa al contexto. El
   * cliente debe ser consciente de las diferencias entre estrategias para tomar
   * la elección correcta.
   */
  const contexto = new Contexto(new EstrategiaConcretaA());
  console.log(
    "Cliente: La estrategia está configurada para ordenamiento normal."
  );
  contexto.hacerAlgoDeLogicaDeNegocio();

  console.log("");

  console.log(
    "Cliente: La estrategia está configurada para ordenamiento inverso."
  );
  contexto.setEstrategia(new EstrategiaConcretaB());
  contexto.hacerAlgoDeLogicaDeNegocio();
}
main();
