/**
 * La interfaz Estrategia declara operaciones comunes a todas las versiones
 * admitidas de algún algoritmo.
 *
 * El Contexto utiliza esta interfaz para llamar al algoritmo definido por Estrategias Concretas.
 */
export interface IEstrategia {
  hacerAlgoritmo(data: string[]): string[];
}
