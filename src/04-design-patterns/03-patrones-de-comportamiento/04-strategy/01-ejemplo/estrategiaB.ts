import { IEstrategia } from "./iEstrategia";

/**
 * Las Estrategias Concretas implementan el algoritmo siguiendo la interfaz base de Estrategia.
 * La interfaz las hace intercambiables en el Contexto.
 */
export class EstrategiaConcretaB implements IEstrategia {
  public hacerAlgoritmo(data: string[]): string[] {
    return data.sort();
  }
}
