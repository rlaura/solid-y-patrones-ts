// Supongamos que tenemos un repositorio de pedidos
import { PedidoRepository } from "./pedidoRepository";
import { Command } from "./icommand";

export class CrearPedidoCommand implements Command {
  constructor(private pedidoRepository: PedidoRepository, private pedidoData: any) {}

  async execute(): Promise<void> {
    const nuevoPedido = await this.pedidoRepository.create(this.pedidoData);
    console.log(`Pedido creado: ${nuevoPedido.id}`);
  }
}