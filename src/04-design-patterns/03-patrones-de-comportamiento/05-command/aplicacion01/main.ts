import { CrearPedidoCommand } from "./crearPedidoCommand";
import { PedidoRepository } from "./pedidoRepository";
/**
 * En el contexto de Domain-Driven Design (DDD), el patrón Command puede ser utilizado, 
 * especialmente en la capa de aplicación y en el diseño de servicios de aplicación. 
 * DDD se centra en el diseño de modelos de dominio ricos y en la comunicación entre las distintas capas de una aplicación, 
 * donde los comandos pueden ser útiles para representar las intenciones del usuario o del sistema.
 * 
 * En DDD, los comandos pueden ser vistos como mensajes que encapsulan una acción específica que se desea realizar en el dominio. 
 * Estos comandos pueden ser enviados desde la capa de presentación o desde otras partes del sistema hacia la capa de aplicación, 
 * donde se procesan y se traducen en acciones que afectan al modelo de dominio.
 * 
 * La capa de aplicación en DDD es responsable de coordinar las operaciones del dominio y 
 * de orquestar la interacción entre las entidadesy los objetos de valor. 
 * Los comandos pueden ser utilizados como una forma de desacoplar la lógica de la aplicación de la lógica de dominio, 
 * lo que permite un mejor diseño y mantenimiento del sistema.
 * 
 * EJEMPLO en el contexto de DOMAIN DRIVEN DESIGN
 * Supongamos que tenemos un escenario donde queremos implementar un sistema de gestión de pedidos. 
 * Utilizaremos el patrón Command para manejar la creación de nuevos pedidos.
 * 
 * - PedidoRepository: es una clase que encapsula la lógica para interactuar con el almacenamiento de pedidos (base de datos, API, etc.).
 * - CrearPedidoCommand: es una clase que implementa la interfaz Command y encapsula la lógica para crear un nuevo pedido 
 *                      utilizando los datos proporcionados.
 * - Cuando se crea una instancia de CrearPedidoCommand y se llama al método execute(), se crea un nuevo pedido utilizando el repositorio de pedidos y los datos proporcionados.
 */
async function main() {
  console.log("================PATRON COMMAND APLICATIVO=================");
  // Supongamos que tenemos una instancia de PedidoRepository
  const pedidoRepository = new PedidoRepository();

  // Datos para el nuevo pedido
  const pedidoData = {
    // Datos del pedido
    laptop:10,
  };

  // Creamos una instancia del Command
  const crearPedidoCommand = new CrearPedidoCommand(
    pedidoRepository,
    pedidoData
  );

  // Ejecutamos el Command
  await crearPedidoCommand.execute();
}

main();
