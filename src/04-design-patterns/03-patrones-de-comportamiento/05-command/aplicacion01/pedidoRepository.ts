export class PedidoRepository {
  async create(pedidoData: any): Promise<{ id: string }> {
    // Lógica para crear un nuevo pedido en el repositorio (base de datos, API, etc.)
    // En este ejemplo, simplemente devolvemos un objeto simulado con un ID
    const nuevoPedido = { id: '123456789', ...pedidoData };
    return nuevoPedido;
  }
}
