/**
 * La interfaz Comando declara un método para ejecutar un comando.
 */
export interface IComando {
  ejecutar(): void;
}