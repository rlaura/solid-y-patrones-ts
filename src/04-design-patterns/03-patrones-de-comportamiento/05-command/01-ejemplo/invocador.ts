import { IComando } from "./icomando";

/**
 * El Invocador está asociado con uno o varios comandos. Envía una solicitud al
 * comando.
 */
export class Invocador {
  private enInicio!: IComando;

  private enFin!: IComando;

  /**
   * Inicializa los comandos.
   */
  public setEnInicio(comando: IComando): void {
      this.enInicio = comando;
  }

  public setEnFin(comando: IComando): void {
      this.enFin = comando;
  }

  /**
   * El Invocador no depende de clases de comando o receptor concretas. El
   * Invocador pasa una solicitud a un receptor indirectamente, ejecutando un
   * comando.
   */
  public hacerAlgoImportante(): void {
      console.log('Invocador: ¿Alguien quiere algo hecho antes de que comience?');
      if (this.esComando(this.enInicio)) {
          this.enInicio.ejecutar();
      }

      console.log('Invocador: ...haciendo algo realmente importante...');

      console.log('Invocador: ¿Alguien quiere algo hecho después de que termine?');
      if (this.esComando(this.enFin)) {
          this.enFin.ejecutar();
      }
  }

  private esComando(objeto: any): objeto is IComando {
      return objeto.ejecutar !== undefined;
  }
}