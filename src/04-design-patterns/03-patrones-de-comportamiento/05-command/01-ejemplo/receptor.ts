/**
 * Las clases Receptor contienen cierta lógica empresarial importante. Saben cómo
 * realizar todo tipo de operaciones asociadas con llevar a cabo una solicitud. De
 * hecho, cualquier clase puede servir como Receptor.
 */
export class Receptor {
  public hacerAlgo(a: string): void {
      console.log(`Receptor: Trabajando en (${a}.)`);
  }

  public hacerOtraCosa(b: string): void {
      console.log(`Receptor: También trabajando en (${b}.)`);
  }
}