import { IComando } from "./icomando";
import { Receptor } from "./receptor";

/**
 * Sin embargo, algunos comandos pueden delegar operaciones más complejas a otros objetos,
 * llamados "receptores".
 */
export class ComandoComplejo implements IComando {
  private receptor: Receptor;

  /**
   * Datos de contexto, requeridos para lanzar los métodos del receptor.
   */
  private a: string;

  private b: string;

  /**
   * Los comandos complejos pueden aceptar uno o varios objetos receptor
   * junto con cualquier dato de contexto a través del constructor.
   */
  constructor(receptor: Receptor, a: string, b: string) {
    this.receptor = receptor;
    this.a = a;
    this.b = b;
  }

  /**
   * Los comandos pueden delegar a cualquier método de un receptor.
   */
  public ejecutar(): void {
    console.log(
      "ComandoComplejo: Cosas complejas deben ser realizadas por un objeto receptor."
    );
    this.receptor.hacerAlgo(this.a);
    this.receptor.hacerOtraCosa(this.b);
  }
}
