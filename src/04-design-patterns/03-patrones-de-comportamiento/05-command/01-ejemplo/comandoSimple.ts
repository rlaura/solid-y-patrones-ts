import { IComando } from "./icomando";

/**
 * Algunos comandos pueden implementar operaciones simples por sí mismos.
 */
export class ComandoSimple implements IComando {
  private carga: string;

  constructor(carga: string) {
    this.carga = carga;
  }

  public ejecutar(): void {
    console.log(
      `ComandoSimple: Verás, puedo hacer cosas simples como imprimir (${this.carga})`
    );
  }
}
