import { ComandoComplejo } from "./comandoComplejo";
import { ComandoSimple } from "./comandoSimple";
import { Invocador } from "./invocador";
import { Receptor } from "./receptor";

/**
 * Este ejemplo fue extraido de 
 * https://refactoring.guru/es/design-patterns/command
 * https://refactoring.guru/es/design-patterns/command/typescript/example
 * 
 * Command es un patrón de diseño de comportamiento que convierte una solicitud en un objeto independiente 
 * que contiene toda la información sobre la solicitud. Esta transformación te permite parametrizar los métodos 
 * con diferentes solicitudes, retrasar o poner en cola la ejecución de una solicitud y soportar operaciones 
 * que no se pueden realizar.
 * 
 */
async function main() {
  /**
   * El código del cliente puede parametrizar un invocador con cualquier comando.
   */
  console.log("================PATRON COMMAND EJEMPLO=================");
  const invocador = new Invocador();
  const commandSimple = new ComandoSimple("¡Di Hola!");
  invocador.setEnInicio(commandSimple);

  const receptor = new Receptor();
  const commandComplejo = new ComandoComplejo(receptor, "Enviar correo electrónico", "Guardar informe");
  invocador.setEnFin(commandComplejo);

  invocador.hacerAlgoImportante();
}

main();
