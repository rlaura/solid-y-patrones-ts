import { ChatRoom } from "./ChatRoom";
import { User } from "./User";
/**
 * En este ejemplo el mediador(ChatRoom) facilita la comunicacion entre los colegas User
 * Cada User puede enviar mensajes a traves del mediator, que luego notifica a todos los otros usuarios conectados.
 * Esto se puede implementar de esta manera si los usuarios necesitan comunicarse 
 * entre si pero prefieren no tener dependencias directas entre ellos
 */
function main() {
  // Ejemplo de uso del patrón Mediator en un entorno empresarial.
  console.log("================PATRON MEDIATOR APLICATIVO=================");
  const chatRoom = new ChatRoom();

  const user1 = new User(chatRoom, "Usuario 1");
  const user2 = new User(chatRoom, "Usuario 2");

  // Los usuarios envían mensajes a través de la sala de chat (Mediator).
  user1.send("¡Hola a todos!");
  user2.send("Hola Usuario 1, ¿cómo estás?");

  // Los mensajes son recibidos por los respectivos usuarios.
  user1.receive("Hola Usuario 1, ¿cómo estás?");
  user2.receive("¡Hola a todos!");
}

main();
