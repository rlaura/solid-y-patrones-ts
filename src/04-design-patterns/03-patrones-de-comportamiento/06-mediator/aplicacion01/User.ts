import { IColleague } from "./IColleague";
import { IMediator } from "./IMediator";

// Implementación concreta de un colega (Usuario).
export class User implements IColleague {
  constructor(private mediator: IMediator, public name: string) {}

  send(message: string): void {
    console.log(`[${this.name}] envía mensaje: "${message}"`);
    this.mediator.notify(this, message);
  }

  receive(message: string): void {
    console.log(`[${this.name}] recibe mensaje: "${message}"`);
  }
}
