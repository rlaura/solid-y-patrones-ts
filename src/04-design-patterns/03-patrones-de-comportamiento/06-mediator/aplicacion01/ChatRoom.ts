import { IColleague } from "./IColleague";
import { IMediator } from "./IMediator";
import { User } from "./User";

// Implementación concreta del Mediator que coordina la comunicación entre colegas.
export class ChatRoom implements IMediator {
  public notify(sender: IColleague, event: string): void {
    console.log(`[${(sender as User).name}] ha enviado el mensaje: "${event}"`);
  }
}
