import { IColleague } from "./IColleague";

// Interfaz para el Mediator que define un método para notificar eventos.
export interface IMediator {
  notify(sender: IColleague, event: string): void;
}
