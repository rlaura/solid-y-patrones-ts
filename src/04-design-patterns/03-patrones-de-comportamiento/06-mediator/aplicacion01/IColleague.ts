// Interfaz para los colegas (componentes) que interactúan con el Mediator.
export interface IColleague {
  send(message: string): void;
  receive(message: string): void;
}
