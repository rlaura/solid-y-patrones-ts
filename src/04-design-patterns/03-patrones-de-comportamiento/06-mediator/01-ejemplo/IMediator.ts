/**
 * La interfaz Mediator declara un método utilizado por los componentes para notificar al
 * mediador sobre varios eventos. El mediador puede reaccionar a estos eventos y
 * pasar la ejecución a otros componentes.
 */
export interface IMediator {
  notify(sender: object, event: string): void;
}
