import { BaseComponent } from "./BaseComponent";
/**
 * Los Componentes Concretos implementan diversas funcionalidades. No dependen de
 * otros componentes. Tampoco dependen de ninguna clase mediadora concreta.
 */
export class Component2 extends BaseComponent {
  public doC(): void {
    console.log("Componente 2 realiza C.");
    this.mediator.notify(this, "C");
  }

  public doD(): void {
    console.log("Componente 2 realiza D.");
    this.mediator.notify(this, "D");
  }
}
