import { BaseComponent } from "./BaseComponent";

/**
 * Los Componentes Concretos implementan diversas funcionalidades. No dependen de
 * otros componentes. Tampoco dependen de ninguna clase mediadora concreta.
 */
export class Component1 extends BaseComponent {
  public doA(): void {
    console.log("Componente 1 realiza A.");
    this.mediator.notify(this, "A");
  }

  public doB(): void {
    console.log("Componente 1 realiza B.");
    this.mediator.notify(this, "B");
  }
}
