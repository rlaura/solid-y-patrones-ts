import { Component1 } from "./Component1";
import { Component2 } from "./Component2";
import { ConcreteMediator } from "./ConcreteMediator";

/**
 * es un patron de comportamiento que ayuda a reducir las dependencias entre objetos 
 * haciendo que se comuniquen indirectamente a travez de un objeto mediador
 */
function main() {
  /**
   * El código del cliente.
   */
  console.log("================PATRON MEDIATOR EJEMPLO=================");
  const component1 = new Component1();
  const component2 = new Component2();
  const mediator = new ConcreteMediator(component1, component2);

  console.log("El cliente desencadena la operación A.");
  component1.doA();

  console.log("");
  console.log("El cliente desencadena la operación D.");
  component2.doD();
}

main();
