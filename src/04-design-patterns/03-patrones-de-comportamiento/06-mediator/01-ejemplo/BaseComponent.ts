import { IMediator } from "./IMediator";

/**
 * El Componente Base proporciona la funcionalidad básica de almacenar la instancia
 * de un mediador dentro de los objetos de componente.
 */
export class BaseComponent {
  protected mediator: IMediator;

  constructor(mediator?: IMediator) {
    this.mediator = mediator!;
  }

  public setMediator(mediator: IMediator): void {
    this.mediator = mediator;
  }
}
