/**
 * REALIZAMOS UN COMPLETO DESARROLLO Y ESTUDIO DE LO QUE SON BUENAS PRACTICAS DE PROGRAMACION
 * https://refactoring.guru/es/design-patterns
 *
 * solid en java
 * https://github.com/alxgcrz/_principios-SOLID_
 *
 * patrones de diseño en java
 * https://github.com/mitocode21/patrones-diseno/tree/master
 * 
 * patrones de diseño en typescript
 * https://github.com/jherr/no-bs-ts/tree/master/series-2/episode-2-pubsub
 * https://www.youtube.com/watch?v=f3Cn0CGytSQ
 *
 */

// export * from "./01-typescript-practice";
// export * from "./02-programacion-orientada-a-objetos";
// export * from "./03-solid";
export * from "./04-design-patterns";
