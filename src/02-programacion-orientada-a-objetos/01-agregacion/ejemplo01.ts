export {};
/**
 * TODO: En la agregación, una clase puede existir independientemente de la otra, 
 * mientras que en la composición, una clase está compuesta por la otra 
 * y no puede existir sin ella.
 */

// Clase representando un Departamento
class Departamento {
  constructor(public nombre: string) {}
}

//TODO: agregación
// Clase representando un Empleado que pertenece a un Departamento
class Empleado {
  constructor(public nombre: string, public departamento: Departamento) {}
}

// Ventajas y Desventajas de la Agregación:
// Ventajas:
// - Mayor flexibilidad, ya que las clases pueden existir independientemente.
// - Se pueden crear múltiples instancias de una clase que hacen referencia a la misma instancia de otra clase.
// - alta cohesion lo que significa menos acoplamiento entre las clases

// Desventajas:
// - Las instancias pueden existir independientemente, lo que puede dificultar el mantenimiento de invariantes.
function main() {
  // Crear una instancia de Departamento
  const departamento = new Departamento("Desarrollo");

  //TODO: agregación
  // Crear una instancia de Empleado que pertenece al Departamento
  //practicamente le estamos inyectando al empleado un departamento
  const empleado = new Empleado("Juan Perez", departamento);

  // La relación entre Empleado y Departamento es de agregación
  // Desarrollo
  console.log(empleado.departamento.nombre);

  // Otro Empleado que pertenece a otro Departamento
  const otroEmpleado = new Empleado(
    "Ana Rodriguez",
    new Departamento("Ventas")
  );
  // Ventas
  console.log(otroEmpleado.departamento.nombre);
}
main();
