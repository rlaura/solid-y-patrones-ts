export {};
/**
 * TODO: En la agregación, una clase puede existir independientemente de la otra,
 * mientras que en la composición, una clase está compuesta por la otra
 * y no puede existir sin ella.
 */

// Clase representando una Rueda
class Rueda {
  constructor(public marca: string) {}
}

//TODO: composición
// Clase representando un Coche que está compuesto por Ruedas
class Coche {
  private ruedas: Rueda[];

  constructor(public modelo: string) {
    this.ruedas = [
      new Rueda("Delantera Izquierda"),
      new Rueda("Delantera Derecha"),
      new Rueda("Trasera Izquierda"),
      new Rueda("Trasera Derecha"),
    ];
  }

  // Método para obtener la información de las ruedas del coche
  obtenerInformacionRuedas() {
    return this.ruedas.map((rueda) => rueda.marca);
  }
}

// Ventajas y Desventajas de la Composición:

// Ventajas:
// - Mayor control sobre las invariantes, ya que las partes están fuertemente vinculadas a la clase que las compone.

// Desventajas:
// - Fuerte acoplamiento, ya que las instancias de la clase compuesta no pueden existir independientemente.
// - Menos flexibilidad, ya que las instancias de la clase compuesta no pueden existir independientemente.
// - Menor reutilización, ya que la clase compuesta está fuertemente vinculada a la clase que la compone.

function main() {
  //TODO: composición
  // El Coche está compuesto por Ruedas
  const coche = new Coche("Audi");
  // [ 'Delantera Izquierda', 'Delantera Derecha', 'Trasera Izquierda', 'Trasera Derecha' ]
  console.log(coche.obtenerInformacionRuedas());
}
main();
