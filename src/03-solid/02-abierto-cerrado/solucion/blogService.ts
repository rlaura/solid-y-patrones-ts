import { HttpClient } from "./httpClient";
/**
 * TODO: Acá estamos haciendo uso de la clase HttpClient lo que significa que
 * ya no dependemos de axios directamente, si no de una clase que lo envuelve y permite
 * encapsular las funciones http
 */
export class TodoService { 
  //TODO: inyeccion de dependencias
  constructor( private http: HttpClient ){}

  async getTodoItems() {
      const { data } = await this.http.get('https://jsonplaceholder.typicode.com/todos/');
      return data;
  }
}


export class PostService {

  constructor( private http: HttpClient ){}

  async getPosts() {
      const { data } = await this.http.get('https://jsonplaceholder.typicode.com/posts');
      return data;
  }
}


export class PhotosService {

  constructor( private http: HttpClient ){}

  async getPhotos() {
      const { data } = await this.http.get('https://jsonplaceholder.typicode.com/photos');
      return data;
  }

}