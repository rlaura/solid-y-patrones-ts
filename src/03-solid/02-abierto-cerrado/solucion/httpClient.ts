// import axios from 'axios';

//TODO: implementar las demas funciones HTTP
//TODO: Aca tenemos una alta cohesion ya que si en un futuro dejeara de funcionar axios
// el cambiar el sistema seria una tarea muy grande, pero de esta manera podriamos
// cambiarlo por fetch o cualquier otra libreria que haga lo mismo
export class HttpClient {
  
  //TODO: Aca por ejemplo usamos axios para el get
  // async get( url: string ) {
  //     const { data, status } = await axios.get(url);
  //     return { data, status };
  // }

  //TODO: Aca por ejemplo usamos fetch para el get
  async get(url: string) {
    const resp = await fetch(url);
    const data = await resp.json();

    return { data, status: resp.status };
  }
}
