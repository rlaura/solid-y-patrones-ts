import axios from "axios";
/**
 * ACA TENEMOS UNA CLASE QUE SIMULA LOS LLAMADOS A UN BLOG
 * TODO: una clase que usa axios de manera muy acoplada y cambiarlo puede ser
 * muy trabajoso para aplicaciones grandes
 */
export class TodoService {
  async getTodoItems() {
    const { data } = await axios.get(
      "https://jsonplaceholder.typicode.com/todos/"
    );
    return data;
  }
}

export class PostService {
  async getPosts() {
    const { data } = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );
    return data;
  }
}

export class PhotosService {
  async getPhotos() {
    const { data } = await axios.get(
      "https://jsonplaceholder.typicode.com/photos"
    );
    return data;
  }
}

export const openAndCloseMain = async () => {
  const todoService = new TodoService();
  const postService = new PostService();
  const photosService = new PhotosService();

  const todos = await todoService.getTodoItems();
  const posts = await postService.getPosts();
  const photos = await photosService.getPhotos();

  console.log({ todos, posts, photos });
};

(() => {
  // openAndCloseMain();
})();
