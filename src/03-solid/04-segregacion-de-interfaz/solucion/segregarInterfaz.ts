//TODO: se separa las diferentes habilidades que puede tener un ave
//en diferentes interfaces, para que las clases de aves solo implementen
//las que sean necesarias

export interface Bird {
  eat(): void;
}

export interface FlyingBird {
  fly(): number;
}

export interface RunningBird {
  run(): void;
}

export interface SwimmerBird {
  swim(): void;
}
