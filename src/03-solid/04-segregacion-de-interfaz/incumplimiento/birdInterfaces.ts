export interface BirdInterface {
  fly(): void;
  eat(): void;
  run(): void;
}