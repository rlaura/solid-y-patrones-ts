import { BirdInterface } from "./birdInterfaces";

class Tucan implements BirdInterface {
  public fly() {}
  public eat() {}
  public run() {}
}

class Humminbird implements BirdInterface {
  public fly() {}
  public eat() {}
  public run() {}
}

//TODO: ACA el avestrus no vuela
// aca se violenta el principo de segregacion de interfaz
class Ostrich implements BirdInterface {
  fly(): void {
    throw new Error("Ostrich no puede volar");
  }
  eat(): void {}
  run(): void {}
}

class Penguin implements BirdInterface {
  fly(): void {
    throw new Error("Penguin no puede volar");
  }
  eat(): void {}
  run(): void {}
  //TODO: Si  agregara esta funcion a la interfaz
  // de bird, solo por agregar este metodo se tendria a fuerza que
  // implementar esta funcion en las demas clases de aves lo cual es inconveniente.
  swim(): void {}
}
