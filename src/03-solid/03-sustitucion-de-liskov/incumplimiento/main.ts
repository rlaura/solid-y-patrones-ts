import { Audi, Honda, Tesla, Toyota, Volvo } from "./automoviles";

(() => {
  //TODO: ACA se tienen que modificar demasiados metodos y clases
  // si se desea crear un nuevo auto llamado volvo
  // lo que indica que la implementacion no es muy optima

  // const printCarSeats = (cars: (Tesla | Audi | Toyota | Honda)[]) => {
  const printCarSeats = (cars: (Tesla | Audi | Toyota | Honda | Volvo)[]) => {
    for (const car of cars) {
      if (car instanceof Tesla) {
        console.log("Tesla", car.getNumberOfTeslaSeats());
        continue;
      }
      if (car instanceof Audi) {
        console.log("Audi", car.getNumberOfAudiSeats());
        continue;
      }
      if (car instanceof Toyota) {
        console.log("Toyota", car.getNumberOfToyotaSeats());
        continue;
      }
      if (car instanceof Honda) {
        console.log("Honda", car.getNumberOfHondaSeats());
        continue;
      }
      //TODO: ACA tambien se tiene que modificar
      if (car instanceof Volvo) {
        console.log("Volvo", car.getNumberOfSeats());
        continue;
      }
    }
  };

  const cars = [
    new Tesla(7),
    new Audi(2),
    new Toyota(5),
    new Honda(5),
    new Volvo(2),
  ];

  printCarSeats(cars);
})();
