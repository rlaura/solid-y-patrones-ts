export class Tesla {
  constructor(private numberOfSeats: number) {}

  getNumberOfTeslaSeats() {
    return this.numberOfSeats;
  }
}

export class Audi {
  constructor(private numberOfSeats: number) {}

  getNumberOfAudiSeats() {
    return this.numberOfSeats;
  }
}

export class Toyota {
  constructor(private numberOfSeats: number) {}

  getNumberOfToyotaSeats() {
    return this.numberOfSeats;
  }
}

export class Honda {
  constructor(private numberOfSeats: number) {}

  getNumberOfHondaSeats() {
    return this.numberOfSeats;
  }
}

//TODO: si trato de agregar un nuevo auto llamado volvo
//aqui no habria mucho problema ya que es solo creo la clase
// y la llamo, pero en el llamado a esta clase surge el problemaF
export class Volvo {
  constructor(private numberOfSeats: number) {}

  getNumberOfSeats() {
    return this.numberOfSeats;
  }
}
