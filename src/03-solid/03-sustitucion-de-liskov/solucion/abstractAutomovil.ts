//TODO: creamos una clase abstracta con un metodo abstracto
// para que las demas clases automoviles la hereden e implementes
// su metodo getNumberOfSeats
// TAMBIEN SE PODRIA USAR UNA INTERFAZ
export abstract class Vehicle {

  // getNumberOfSeats():number {
  //     throw Error('Method not implemented');
  // }
  abstract getNumberOfSeats(): number;

}