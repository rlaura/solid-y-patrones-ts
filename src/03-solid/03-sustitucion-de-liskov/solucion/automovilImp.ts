import { Vehicle } from "./abstractAutomovil";
//TODO: extendemos de la clase abstracta Vehicle e implemenatmos sus metodos
export class Tesla extends Vehicle {
  constructor(private numberOfSeats: number) {
    //super(): se utiliza en una clase hija para llamar al constructor de la clase padre.
    //Esto es útil cuando se desea heredar los atributos y métodos de la clase padre en la clase hija
    //En este caso el constructor de la clase padre no tiene ningun atributo
    super();
  }

  getNumberOfSeats() {
    return this.numberOfSeats;
  }
}

export class Audi extends Vehicle {
  constructor(private numberOfSeats: number) {
    super();
  }

  getNumberOfSeats() {
    return this.numberOfSeats;
  }
}

export class Toyota extends Vehicle {
  constructor(private numberOfSeats: number) {
    super();
  }

  getNumberOfSeats() {
    return this.numberOfSeats;
  }
}

export class Honda extends Vehicle {
  constructor(private numberOfSeats: number) {
    super();
  }

  getNumberOfSeats() {
    return this.numberOfSeats;
  }
}

export class Volvo extends Vehicle {
  constructor(private numberOfSeats: number) {
    super();
  }

  getNumberOfSeats() {
    return this.numberOfSeats;
  }
}

export class Ford extends Vehicle {
  constructor(private numberOfSeats: number) {
    super();
  }

  getNumberOfSeats() {
    return this.numberOfSeats;
  }
}
