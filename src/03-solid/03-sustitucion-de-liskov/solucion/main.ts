import { Vehicle } from "./abstractAutomovil";
import { Audi, Ford, Honda, Tesla, Toyota, Volvo } from "./automovilImp";

(() => {
  const printCarSeats = (cars: Vehicle[]) => {
    //TODO: ACA usamos un forEach para recorrer los arreglos de los vehiculos
    // hacemos uso de car.constructor.name que llama el nombre del constructor
    // SI COMPARAMOS CON LO DE ABAJO ESTA ES UNA MEJOR SOLUCION
    cars.forEach((car) => {
      console.log(car.constructor.name, car.getNumberOfSeats());
    });

    //TODO: aca estamos violando el principio de open and close,
    // ya que tenemos que crear un nuevo if para cuando se
    // cree un nuevo vehiculo y asi si se crean nuevos vehiculos.
    // for (const car of cars) {

    // if( car instanceof Tesla ) {
    //     console.log( 'Tesla', car.getNumberOfSeats() )
    //     continue;
    // }
    // if( car instanceof Audi ) {
    //     console.log( 'Audi', car.getNumberOfSeats() )
    //     continue;
    // }
    // if( car instanceof Toyota ) {
    //     console.log( 'Toyota', car.getNumberOfSeats() )
    //     continue;
    // }
    // if( car instanceof Honda ) {
    //     console.log( 'Honda', car.getNumberOfSeats() )
    //     continue;
    // }

    // if( car instanceof Volvo ) {
    //     console.log( 'Volvo', car.getNumberOfSeats() )
    //     continue;
    // }

    // if( car instanceof Ford ) {
    //     console.log( 'Ford', car.getNumberOfSeats() )
    //     continue;
    // }

    // }
  };

  const cars = [
    new Tesla(7),
    new Audi(2),
    new Toyota(5),
    new Honda(5),
    new Volvo(2),
    new Ford(2),
  ];

  printCarSeats(cars);
})();
