interface Product {
  id: number;
  name: string;
}

// Usualmente, esto es una clase para controlar la vista que es desplegada al usuario
// Recuerden que podemos tener muchas vistas que realicen este mismo trabajo.
//TODO: Solo se encarga del manejo del PRODUCTO
export class ProductBloc {
  private productService: ProductService;
  private mailer: Mailer;

  //TODO: Aca hacemos inyeccion de dependencias
  constructor(productService: ProductService, mailer: Mailer) {
    this.productService = productService;
    this.mailer = mailer;
  }

  // Realiza un proceso para obtener el producto y retornarlo
  loadProduct(id: number) {
    this.productService.getProduct(id);
  }

  // Realiza una petición para salvar en base de datos
  saveProduct(product: Product) {
    this.productService.saveProduct(product);
  }

  // Notifica a los clientes
  notifyClients() {
    this.mailer.sendEmail(["eduardo@gmail.com"], "to-clients");
  }
}
/************************************************************************************** */
//TODO: se encarga solo de traer los datos de la base de datos... solo es una suposición para el ejempl
class ProductService {
  getProduct(id: number) {
    console.log("Producto: ", { id, name: "OLED Tv" });
  }

  saveProduct(product: Product) {
    console.log("Guardando en base de datos", product);
  }
}
/************************************************************************************** */
//TODO: se encarga solo de manejar el mailer
class Mailer {
  private masterEmail: string = "rlaura@gmail.com";

  sendEmail(emailList: string[], template: "to-clients" | "to-admins") {
    console.log("Enviando correo a los clientes", template);
  }
}
/************************************************************************************** */
//TODO: se encarga solo del card
class CartBloc {
  private itemsInCart: Object[] = [];
  // Agregar al carrito de compras
  addToCart(productId: number) {
    console.log("Agregando al carrito ", productId);
  }
}
/************************************************************************************** */
console.log("✅ INCUMPLIMIENTO DEL PRINCIPIO DE RESPONSABILIDAD UNICA");
const productService = new ProductService();
const mailer = new Mailer();

/**
 * TODO: El hacerlo de esta forma permite poder realizar mocks de la clase ProductService y Mailer
 * y de esta forma solo hacer testing de la clase ProductBloc esto gracias a la inyeccion de dependencias
 */
const productBloc = new ProductBloc(productService, mailer);
const cartBloc = new CartBloc();

productBloc.loadProduct(10);
productBloc.saveProduct({ id: 10, name: "OLED TV" });
productBloc.notifyClients();
cartBloc.addToCart(10);
