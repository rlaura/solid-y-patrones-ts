interface Product {
  id: number;
  name: string;
}

/**
 * Aca tenemos un clase productBloc que tiene varias funciones pero
 * se puede ver hay una función onAddToCart que no se ve muy bien en la
 * clase productBloc ya que agrega al cliente al carrito de compras
 * y esto se debería separar
 */
// Usualmente, esto es una clase para controlar la vista que es desplegada al usuario
// Recuerden que podemos tener muchas vistas que realicen este mismo trabajo.
export class ProductBlocI {
  // Realiza un proceso para obtener el producto y retornarlo
  loadProduct(id: number) {
    console.log("Producto: ", { id, name: "OLED Tv" });
  }

  // Realiza una petición para salvar en base de datos
  saveProduct(product: Product) {
    console.log("Guardando en base de datos", product);
  }

  // Notifica a los clientes
  notifyClients() {
    console.log("Enviando correo a los clientes");
  }

  // Agregar al carrito de compras
  onAddToCart(productId: number) {
    console.log("Agregando al carrito ", productId);
  }
}
console.log("❌ INCUMPLIMIENTO DEL PRINCIPIO DE RESPONSABILIDAD UNICA");
const productBloc = new ProductBlocI();

productBloc.loadProduct(10);
productBloc.saveProduct({ id: 10, name: "OLED TV" });
productBloc.notifyClients();
productBloc.onAddToCart(10);
