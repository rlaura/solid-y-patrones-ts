import { WebApiPostService } from "./LocalDataBaseService";
import { PostService } from "./PostService";

// Main
(async () => {

  console.log("✅ CUMPLIMIENTO DEL PRINCIPIO DE INVERSION DE DEPENDENCIAS")
  /**
   * TODO: Ahora realizar un cambio simplemente se podria hacer con
   * crear una nueva instancia de JsonDataBaseService() si tenemos que usar el json, o
   * crear una nueva instancia de WebApiPostService() si ya tenemos implementado el llamado a la api
   * 
   */
  // const provider = new LocalDataBaseService();
  // const provider = new JsonDataBaseService();
  const provider = new WebApiPostService();

  const postService = new PostService( provider );

  const posts = await postService.getPosts();

  console.log({ posts });


})();