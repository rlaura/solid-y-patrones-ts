import { PostProviderAbstract } from "./LocalDataBaseService";

export interface Post {
  body:   string;
  id:     number;
  title:  string;
  userId: number;
}


export class PostService {

  private posts: Post[] = [];
  /**
   * TODO: Aca llamamos a PostProviderAbstract que es una CLASE ABSTRACTA LO QUE IMPLICA QUE LA CLASE NO
   * CONOCE LA IMPLEMENTACION Y QUE DESDE EL MAIN SE ENVIARA, MEDIANTE INYECCION DE DEPENDENCIAS
   * EL TIPO DE IMPLEMENTACION QUE USARA, LOS CUALES PUEDEN SER:
   * - LocalDataBaseService
   * - JsonDataBaseService
   * - WebApiPostService
   */
  constructor( private postProvider: PostProviderAbstract ) {}

  async getPosts() {
      
      this.posts = await this.postProvider.getPosts();

      return this.posts;
  }
}