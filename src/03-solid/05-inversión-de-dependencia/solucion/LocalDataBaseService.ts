import localPosts from "../data-mock/local-database.json";
import { Post } from "./PostService";
/**
 * TODO: Aca usamos la SUSTITUCION DE LISKOV para abstraer
 * el metodo y dejar que las clases que hereden de PostProviderAbstract e
 * implementen su metodo getPosts
 * TAMBIEN SE PUEDE USAR UNA INTERFAZ
 */
export abstract class PostProviderAbstract {
  abstract getPosts(): Promise<Post[]>;
}
//TODO: aca usamos data fake creada
export class LocalDataBaseService implements PostProviderAbstract {
  async getPosts() {
    return [
      {
        userId: 1,
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        userId: 1,
        id: 2,
        title: "qui est esse",
        body: "est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla",
      },
    ];
  }
}
//TODO: aca usamos data fake que nosotros creamos en un json
export class JsonDataBaseService implements PostProviderAbstract {
  async getPosts() {
    return localPosts;
  }
}
//TODO: Aca llamamos a la api directamente, cuando ya este listo el backend
export class WebApiPostService implements PostProviderAbstract {
  async getPosts(): Promise<Post[]> {
    const resp = await fetch("https://jsonplaceholder.typicode.com/posts");
    return await resp.json();
  }
}
// https://jsonplaceholder.typicode.com/posts
