import { PostService } from "./PostService";

// Main
(async () => {
  console.log("❌ INCUMPLIMIENTO DEL PRINCIPIO DE INVERSION DE DEPENDENCIA");
  //TODO: DEMASIADO ACOPLADO A UNA SOLA CLASE
  const postService = new PostService();

  const posts = await postService.getPosts();

  console.log({ posts });
  console.log("/******************************************************************************/");
})();
