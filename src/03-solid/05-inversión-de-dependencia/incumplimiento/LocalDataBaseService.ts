/**
 * TODO: Aca tenemos un problema ya que si suponemos que somos desarrolladores
 * y que el backend todavia no esta listo usamos esta clase para simular quetenemos datos
 * y por ende usamos su metodo getFakePosts, ahora que pasa si queremos 
 * cambiar este metodo por otro nombre como getFakePostsDatabase, se tendria 
 * que cambiar tambien las clases que lo exporten
 */
export class LocalDataBaseService {
  constructor() {}

  async getFakePosts() {
    return [
      {
        userId: 1,
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        userId: 1,
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        userId: 1,
        id: 2,
        title: "qui est esse",
        body: "est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla",
      },
    ];
  }
}
