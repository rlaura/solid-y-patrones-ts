import { LocalDataBaseService } from "./LocalDataBaseService";

export interface Post {
  body: string;
  id: number;
  title: string;
  userId: number;
}

export class PostService {
  private posts: Post[] = [];

  constructor() {}

  async getPosts() {
    //TODO: DEMASIADO ACOPLADO A UNA SOLA CLASE
    const jsonDB = new LocalDataBaseService();
    //TODO: si trato de cambiar los nombres de los metodos entonces si se puede porque solo es un metodo
    // pero que pasa si fuera un proyecto y esto se llamara en mas de 30 sitios, cambiarlo sria tedioso
    // en este caso se estaria bulnerando el PRINCIPIO DE OPEN AND CLOSE
    this.posts = await jsonDB.getFakePosts();

    return this.posts;
  }
}
