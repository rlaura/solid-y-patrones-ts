export {};
/**
 * TODO: La "Type Assertion" en TypeScript es una forma de indicar al compilador
 * que sabes más sobre el tipo de un valor que lo que TypeScript puede inferir
 * de forma automática. Es similar a un cast en otros lenguajes de programación
 * TODO: "Confía en mí, sé que este valor es de cierto tipo".
 */
function main() {
  // Supongamos que tenemos un valor de tipo 'any'
  let valor1: any = "Hola, mundo";

  // Type assertion usando 'as'
  let longitud1: number = (valor1 as string).length;

  // Supongamos que tenemos un valor de tipo 'any'
  let valor2: any = "Hola, mundo";

  // Type assertion usando la notación de ángulos
  let longitud2: number = (<string>valor2).length;
}
main();
