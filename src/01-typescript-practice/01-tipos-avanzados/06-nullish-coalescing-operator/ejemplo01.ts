export {};
/**
 * TODO: Este operador (??) se utiliza para proporcionar un valor de respaldo (fallback)
 * en caso de que el valor de la izquierda sea null o undefined,
 * pero no en caso de que sea un valor falsy como 0, "", false, etc.
 */
/**
 * TODO: su uso se hace usual en las variables .env
 */
function main() {
  const nombre = null;
  const nombrePredeterminado = "Invitado";

  // Uso de Nullish Coalescing Operator
  const nombreFinal = nombre ?? nombrePredeterminado;
  // Resultado: "Invitado"
  console.log(nombreFinal);

  const cantidad = 0;
  const cantidadPredeterminada = 42;
  // Uso de Nullish Coalescing Operator
  const cantidadFinal = cantidad ?? cantidadPredeterminada;
  // Resultado: 0 (ya que 0 no es null ni undefined)
  console.log(cantidadFinal);
}
main();
