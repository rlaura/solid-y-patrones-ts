/**
 * TODO: porque cuando comento el export interface no me funciona
 * y cuando lo dejo descomentado si me funciona
 */
// export interface nada {};
/**
 * TODO: cuando usamos export e import hacemos del archivo un modulo
 * Esta declaración vacía de exportación indica a TypeScript que
 * este archivo es un módulo, aunque no esté exportando nada en particular.
 *
 */
export {};
// Definición de tipos
type EmpleadoTiempoCompleto = {
  tipo: "tiempoCompleto";
  nombre: string;
  salarioAnual: number;
};

type EmpleadoTiempoParcial = {
  tipo: "tiempoParcial";
  nombre: string;
  horasPorSemana: number;
  salarioPorHora: number;
};

// Union Type para representar ambos tipos de empleados
type Empleado = EmpleadoTiempoCompleto | EmpleadoTiempoParcial;

// Función que trabaja con Union Type
function calcularSalarioTotal(empleado: Empleado): number {
  if (empleado.tipo === "tiempoCompleto") {
    return empleado.salarioAnual;
  } else {
    return empleado.horasPorSemana * empleado.salarioPorHora * 52; // Suponiendo 52 semanas en un año
  }
}

function main() {
  // Ejemplos de instancias de empleados
  const empleadoCompleto: Empleado = {
    tipo: "tiempoCompleto",
    nombre: "Juan Perez",
    salarioAnual: 60000,
  };

  const empleadoParcial: Empleado = {
    tipo: "tiempoParcial",
    nombre: "Ana Rodriguez",
    horasPorSemana: 20,
    salarioPorHora: 15,
  };

  // Uso de la función
  const salarioTotalCompleto = calcularSalarioTotal(empleadoCompleto);
  console.log(
    `Salario total del empleado a tiempo completo: ${salarioTotalCompleto}`
  );

  const salarioTotalParcial = calcularSalarioTotal(empleadoParcial);
  console.log(
    `Salario total del empleado a tiempo parcial: ${salarioTotalParcial}`
  );
}
main();
