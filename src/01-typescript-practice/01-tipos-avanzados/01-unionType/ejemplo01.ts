/**
 * TODO: porque cuando comento el export interface no me funciona
 * y cuando lo dejo descomentado si me funciona
 */
// export interface nada {};
/**
 * TODO: cuando usamos export e import hacemos del archivo un modulo
 * Esta declaración vacía de exportación indica a TypeScript que
 * este archivo es un módulo, aunque no esté exportando nada en particular.
 *
 */
export {};

// Definimos un tipo unión con dos posibles tipos: string o number
type ID = string | number;

// Función que acepta un parámetro de tipo ID
function imprimirID(id: ID) {
  console.log(`ID: ${id}`);
}

function main() {
  // Ejemplos de uso
  imprimirID("123");
  imprimirID(456);
  //da error
  // imprimirID(true);
}
main();
