export {};
/**
 * TODO:  A diferencia de any, unknown es un tipo más seguro porque
 * no permite la realización de operaciones arbitrarias en él
 * sin antes realizar una verificación de tipo o una reducción segura de tipo.
 */
//TODO: debes realizar comprobaciones de tipo antes de trabajar con él.
//TODO: Verificación de Tipo y Narrowing con unknown
function imprimirTexto(valor: unknown) {
  // Dentro de este bloque, TypeScript sabe que 'valor' es de tipo 'string'
  if (typeof valor === "string") {
    console.log(valor.toUpperCase());
  } else {
    console.log("No es una cadena");
  }
}

function main() {
  //TODO: Declaración y Asignación de unknown
  let variableDesconocida: unknown;
  variableDesconocida = 42;
  variableDesconocida = "Hola, mundo";
  variableDesconocida = [1, 2, 3];

  // Salida: HOLA
  imprimirTexto("Hola");
  // Salida: No es una cadena
  imprimirTexto(42);

  //TODO: Type Assertion con unknown
  let variableDesconocida2: unknown = "Hola, mundo";
  // Type assertion, indicando al compilador que confiamos en que la variable es de tipo 'string'
  let longitud: number = (variableDesconocida2 as string).length;
  // Salida: 12
  console.log(longitud);
}
main();
