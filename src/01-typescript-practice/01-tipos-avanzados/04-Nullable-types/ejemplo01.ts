export {};
/**
 * TODO: Los tipos nulos en TypeScript se manejan mediante 
 * la adición del modificador null o undefined a un tipo existente. 
 */

// Variable que puede ser una cadena o nula
let mensaje: string | null;

mensaje = "Hola, mundo"; // Válido
mensaje = null; // Válido
// mensaje = undefined;   // Error, el valor no está en el conjunto definido

/************************************************************************** */
// Variable que puede ser un número o indefinida
let cantidad: number | undefined;

cantidad = 42; // Válido
cantidad = undefined; // Válido
// cantidad = null;      // Error, el valor no está en el conjunto definido

/************************************************************************** */
// Función que acepta una cadena o es nula
function imprimirMensaje(mensaje: string | null) {
  if (mensaje !== null) {
    console.log(mensaje);
  } else {
    console.log("El mensaje es nulo.");
  }
}

function main() {
  imprimirMensaje("Hola"); // Válido
  imprimirMensaje(null); // Válido
  // imprimirMensaje(undefined);// Error, el valor no está en el conjunto definido
}
main();
