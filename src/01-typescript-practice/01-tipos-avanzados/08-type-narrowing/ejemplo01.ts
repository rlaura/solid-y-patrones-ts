export {};
/**
 * TODO: Type narrowing (reducción de tipo) en TypeScript
 * se refiere a técnicas que permiten al compilador entender
 * y reducir el tipo de una variable en función de ciertas
 * condiciones lógicas en el código.
 */
/**************************************************************** */
//TODO: Uso de typeof para narrowing de tipos
function imprimirMensaje(mensaje: string | number) {
  if (typeof mensaje === "string") {
    // Aquí, TypeScript reduce automáticamente el tipo de 'mensaje' a 'string'
    console.log("El mensaje es una cadena:", mensaje.toUpperCase());
  } else {
    // Aquí, TypeScript sabe que 'mensaje' no puede ser una cadena, por lo que debe ser un número
    console.log("El mensaje es un número:", mensaje.toFixed(2));
  }
}
/****************************************************************** */
class Animal {
  sonido() {
    console.log("Hace un sonido");
  }
}

class Perro extends Animal {
  ladrar() {
    console.log("¡Guau!");
  }
}
//TODO: Uso de instanceof para narrowing de tipos
function hacerSonido(animal: Animal | Perro) {
  // Aquí, TypeScript reduce el tipo de 'animal' a 'Perro'
  if (animal instanceof Perro) {
    animal.ladrar();
  } else {
    // Aquí, TypeScript sabe que 'animal' es de tipo 'Animal'
    animal.sonido();
  }
}
/************************************************************************* */
interface Cuadrado {
  tipo: "cuadrado";
  lado: number;
}

interface Circulo {
  tipo: "círculo";
  radio: number;
}

//TODO: Uso de discriminantes de tipo (Type Discriminants)
function areaFigura(figura: Cuadrado | Circulo) {
  switch (figura.tipo) {
    case "cuadrado":
      // Aquí, TypeScript reduce el tipo de 'figura' a 'Cuadrado'
      return figura.lado * figura.lado;
    case "círculo":
      // Aquí, TypeScript reduce el tipo de 'figura' a 'Circulo'
      return Math.PI * figura.radio ** 2;
  }
}
/************************************************************************* */
function main() {
  // Salida: El mensaje es una cadena: HOLA
  imprimirMensaje("Hola");
  // Salida: El mensaje es un número: 42.00
  imprimirMensaje(42);

  /************************************************************ */
  const miAnimal = new Animal();
  const miPerro = new Perro();
  // Salida: Hace un sonido
  hacerSonido(miAnimal);
  // Salida: ¡Guau!
  hacerSonido(miPerro);

  const miCuadrado: Cuadrado = { tipo: "cuadrado", lado: 5 };
  const miCirculo: Circulo = { tipo: "círculo", radio: 3 };

  // Salida: 25
  console.log(areaFigura(miCuadrado));
  // Salida: ~28.27
  console.log(areaFigura(miCirculo));
}
main();
