export {};
/**
 * TODO:
 * El Optional Chaining es una característica de TypeScript (y JavaScript)
 * que permite acceder a las propiedades de un objeto incluso si alguna de
 * las propiedades intermedias es null o undefined. Esta característica es
 * especialmente útil para evitar errores de tipo TypeError al intentar acceder
 * a propiedades de objetos anidados cuando alguna de ellas es nula o indefinida.
 */
/**
 * TODO: La sintaxis del Optional Chaining es agregar un signo de interrogación (?)
 * después de la propiedad que podría ser nula o indefinida.
 */
// Ejemplo de objeto anidado
const persona = {
  nombre: "Juan",
  direccion: {
    ciudad: "Barcelona",
    codigoPostal: "08001",
  },
};
//TODO: uso en metodos
const objetoConMetodo = {
  metodo: () => "Hola desde el método",
};

function main() {
  // Uso de Optional Chaining
  const codigoPostal = persona?.direccion?.codigoPostal;
  // Puede ser un número o undefined si alguna propiedad es nula
  console.log(codigoPostal);

  //TODO: Uso de Optional Chaining con método
  const resultado = objetoConMetodo?.metodo?.();
  // Puede ser el resultado del método o undefined si el método no existe
  console.log(resultado);

  //TODO: Uso con Arrays
  const lista = [1, 2, 3, 4];
  //TODO:  Uso de Optional Chaining con array
  const primerElemento = lista?.[0];
  // Puede ser el primer elemento o undefined si la lista es null o está vacía
  console.log(primerElemento);
}
main();
