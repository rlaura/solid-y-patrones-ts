export {};
/**
 * TODO: 
 * los Intersection Types (tipos intersección) permiten combinar varios tipos en uno solo. 
 * Puedes usar el símbolo & para combinar dos o más tipos en un nuevo tipo que tenga todas 
 * las propiedades de los tipos originales.
 */
// Definimos dos tipos: Persona y Empleado
type Persona = {
  nombre: string;
  edad: number;
};

type Empleado = {
  trabajo: string;
  salario: number;
};

//TODO: Definimos un tipo intersección que combina las propiedades de Persona y Empleado
type PersonaEmpleado = Persona & Empleado;

// Creamos una instancia de PersonaEmpleado
const personaEmpleado: PersonaEmpleado = {
  nombre: "Juan",
  edad: 30,
  trabajo: "Desarrollador",
  salario: 50000,
};

// Función que acepta un parámetro de tipo PersonaEmpleado
function imprimirInformacion(personaEmpleado: PersonaEmpleado) {
  console.log(`Nombre: ${personaEmpleado.nombre}`);
  console.log(`Edad: ${personaEmpleado.edad}`);
  console.log(`Trabajo: ${personaEmpleado.trabajo}`);
  console.log(`Salario: ${personaEmpleado.salario}`);
}

// Llamamos a la función con la instancia de PersonaEmpleado
imprimirInformacion(personaEmpleado);
