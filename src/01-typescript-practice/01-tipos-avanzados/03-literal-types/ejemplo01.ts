export {};

type EstadoCivil = "soltero" | "casado" | "divorciado" | "viudo";

function main() {
  // Uso del tipo literal de cadena
  let estado: EstadoCivil;

  estado = "soltero"; // Válido
  // estado = "complicado"; // Error, el valor no está en el conjunto definido
  console.log(estado);
}
main();
