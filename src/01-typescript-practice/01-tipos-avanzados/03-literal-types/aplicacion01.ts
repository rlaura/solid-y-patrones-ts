export {};
// Tipo literal para representar estados de un pedido
type EstadoPedido = "pendiente" | "en_proceso" | "completo" | "cancelado";

// Interfaz para representar un pedido
interface Pedido {
  id: number;
  descripcion: string;
  estado: EstadoPedido;
}

// Función que actualiza el estado de un pedido
function actualizarEstadoPedido(
  pedido: Pedido,
  nuevoEstado: EstadoPedido
): Pedido {
  return { ...pedido, estado: nuevoEstado };
}

function main() {
  // Ejemplo de uso
  const pedidoInicial: Pedido = {
    id: 1,
    descripcion: "Producto A",
    estado: "pendiente",
  };

  // Actualizar el estado del pedido
  const pedidoActualizado: Pedido = actualizarEstadoPedido(
    pedidoInicial,
    "en_proceso"
  );

  console.log(`Estado del pedido actualizado: ${pedidoActualizado.estado}`);
}
main();
